Что выведет программа? Объяснить вывод программы.

```go
package main

type customError struct {
	msg string
}

func (e *customError) Error() string {
	return e.msg
}

func test() *customError {
	{
		// do something
	}
	return nil
}

func main() {
	var err error
	err = test()
	if err != nil {
		println("error")
		return
	}
	println("ok")
}
```

Ответ:
```
Вывод: error. Причина: мы возвращаем указатель на ошибку и привязываем его к интерфейсу error. Сам указатель не nil,
он лишь указывает на nil, из-за чего сравнение err и nil дает false 

```
