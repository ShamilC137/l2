Что выведет программа? Объяснить вывод программы.

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for {
			select {
			case v := <-a:
				c <- v
			case v := <-b:
				c <- v
			}
		}
	}()
	return c
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4 ,6, 8)
	c := merge(a, b )
	for v := range c {
		fmt.Println(v)
	}
}
```

Ответ:
```
Вывод: числа в слайсах и бесконечные 0. 
   
Причина: функция merge написана некорректно. Если закрыть канал, то он будет всегда 
готов к "чтению", что будет триггерить select блок, возвращая дефолтное значение и false для флага ok (v, ok := <-ch), 
из-за чего возникнет бесконечный цикл в функции merge. 

Также не закрывается канал, возвращаемый функцией merge, что приведен к бесконечному ожиданию в range-based-for, если исправить 
бесконечный цикл в select.
```
