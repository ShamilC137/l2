package pattern

import "C"
import (
	"syscall"
	"unsafe"
)

/*
	Реализовать паттерн «фасад».
Объяснить применимость паттерна, его плюсы и минусы,а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Facade_pattern
*/

type Socket struct {
	shutdown bool
	fd       syscall.Handle
}

func NewSocket(host string, port string) (socket *Socket, err error) {
	fd, err := syscall.Socket(syscall.AF_INET, syscall.SOCK_STREAM, syscall.IPPROTO_TCP)
	if err != nil {
		return
	}

	hints := syscall.AddrinfoW{Family: syscall.AF_INET, Socktype: syscall.SOCK_STREAM, Protocol: syscall.IPPROTO_TCP, Flags: syscall.AI_PASSIVE}
	addrinfo := &syscall.AddrinfoW{}

	realHost, err := syscall.UTF16FromString(host)
	if err != nil {
		return
	}

	realPort, err := syscall.UTF16FromString(port)
	if err != nil {
		return
	}
	err = syscall.GetAddrInfoW((*uint16)(unsafe.Pointer(&realHost)), (*uint16)(unsafe.Pointer(&realPort)), &hints, &addrinfo)
	if err != nil {
		return
	}

	ptr := addrinfo
	for ; ptr != nil; ptr = ptr.Next {
		raw := (*syscall.RawSockaddrInet4)(unsafe.Pointer(ptr.Addr))
		err = syscall.Bind(fd, &syscall.SockaddrInet4{Port: int(raw.Port), Addr: raw.Addr})
		if err != nil {
			break
		}
	}

	syscall.FreeAddrInfoW(addrinfo)

	if ptr == nil {
		_ = syscall.Closesocket(fd)
		return
	}

	err = syscall.Listen(fd, syscall.SOMAXCONN)
	if err != nil {
		return
	}

	socket = &Socket{false, fd}
	return
}

func (socket *Socket) Accept() (result *Socket, err error) {
	fork, _, err := syscall.Accept(socket.fd)
	if err != nil {
		return
	}

	result = &Socket{false, fork}
	return
}

func (socket *Socket) Close() error {
	if socket.fd < 0 {
		return nil
	}
	err := syscall.Closesocket(socket.fd)
	if err != nil {
		return err
	}

	socket.fd = -1
	return nil
}

func (socket *Socket) Shutdown() error {
	if !socket.shutdown {
		err := syscall.Shutdown(socket.fd, syscall.SHUT_WR)
		if err != nil {
			return err
		}
		socket.shutdown = true
	}

	return nil
}
