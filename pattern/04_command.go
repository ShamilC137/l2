package pattern

import (
	"errors"
	"fmt"
	"sync"
	"sync/atomic"
)

/*
	Реализовать паттерн «комманда».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Command_pattern
*/

type Command interface {
	Execute() error
}

type CommandQueue struct {
	commands []Command
	guard    *sync.Mutex
	cond     *sync.Cond
	closed   *int32
}

var ErrQueueClosed = errors.New("queue is closed")

func (queue *CommandQueue) Push(command Command) error {
	if atomic.LoadInt32(queue.closed) == 1 {
		return ErrQueueClosed
	}

	queue.guard.Lock()
	defer queue.guard.Unlock()

	queue.commands = append(queue.commands, command)
	queue.cond.Signal()
	return nil
}

func (queue *CommandQueue) Pop() (res Command, err error) {
	if atomic.LoadInt32(queue.closed) == 1 {
		err = ErrQueueClosed
		return
	}

	queue.guard.Lock()
	defer queue.guard.Unlock()

	for atomic.LoadInt32(queue.closed) == 0 && len(queue.commands) == 0 {
		queue.cond.Wait()
	}

	if atomic.LoadInt32(queue.closed) == 1 {
		err = ErrQueueClosed
		return
	}

	last := len(queue.commands) - 1
	res = queue.commands[last]
	queue.commands = queue.commands[:last]

	return
}

func (queue *CommandQueue) Close() {
	atomic.StoreInt32(queue.closed, 1)
}

func NewCommandQueue() *CommandQueue {
	mutex := new(sync.Mutex)

	return &CommandQueue{make([]Command, 0, 8), mutex, sync.NewCond(mutex), new(int32)}
}

var queue = NewCommandQueue()

type CommandFunc func() error

func (function CommandFunc) Execute() error {
	return function()
}

func CommandSender() {
	for index := 0; index < 10; index++ {
		_ = queue.Push(CommandFunc(func() error {
			fmt.Println(index)
			return nil
		}))
	}

	// whatever happens
	queue.Close()
}

func CommandReceiver() {
	for command, err := queue.Pop(); err != ErrQueueClosed; command, err = queue.Pop() {
		command()
	}
}
