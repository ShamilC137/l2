package pattern

import (
	"encoding/json"
	"io"
)

/*
	Реализовать паттерн «посетитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Visitor_pattern
*/

type Visitor interface {
	VisitDelivery(delivery *Delivery) error
	VisitItem(item *Item) error
}

type Visitable interface {
	Accept(Visitor) error
}

type Delivery struct {
	Name    *string `json:"name" validate:"required"`
	Phone   *string `json:"phone" validate:"required"`
	Zip     *string `json:"zip" validate:"required"`
	City    *string `json:"city" validate:"required"`
	Address *string `json:"address" validate:"required"`
	Region  *string `json:"region" validate:"required"`
	Email   *string `json:"email" validate:"required"`
}

func (delivery *Delivery) Accept(visitor Visitor) error {
	return visitor.VisitDelivery(delivery)
}

type Item struct {
	ChrtId      *uint   `json:"chrt_id" validate:"required"`
	TrackNumber *string `json:"track_number" validate:"required"`
	Price       *uint   `json:"price" validate:"required"`
	Rid         *string `json:"rid" validate:"required"`
	Name        *string `json:"name" validate:"required"`
	Sale        *uint   `json:"sale" validate:"required"`
	Size        *string `json:"size" validate:"required"`
	TotalPrice  *uint   `json:"total_price" validate:"required"`
	NmId        *uint   `json:"nm_id" validate:"required"`
	Brand       *string `json:"brand" validate:"required"`
	Status      *uint   `json:"status" validate:"required"`
}

func (item *Item) Accept(visitor Visitor) error {
	return visitor.VisitItem(item)
}

type Serializer struct {
	writer io.ByteWriter
}

func (serializer *Serializer) SimpleSerialization(value any) error {
	data, err := json.Marshal(value)
	if err != nil {
		return err
	}

	for _, val := range data {
		err = serializer.writer.WriteByte(val)
		if err != nil {
			return err
		}
	}

	return nil
}

func (serializer *Serializer) VisitItem(item *Item) error {
	return serializer.SimpleSerialization(item)
}

func (serializer *Serializer) VisitDelivery(delivery *Delivery) error {
	return serializer.SimpleSerialization(delivery)
}
