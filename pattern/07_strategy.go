package pattern

import "fmt"

/*
	Реализовать паттерн «стратегия».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Strategy_pattern
*/

type cacheElemInfo struct {
	value  any
	used   uint32
	marked bool
}

type ReplacementAlgo interface {
	Replace(cache *Cache, key any, value any)
}

type Cache struct {
	data            map[any]*cacheElemInfo
	capacity        uint32
	maxCapacity     uint32
	replacementAlgo ReplacementAlgo
}

func NewCache(algo ReplacementAlgo) *Cache {
	return &Cache{
		data:            make(map[any]*cacheElemInfo, 8),
		capacity:        0,
		maxCapacity:     8,
		replacementAlgo: algo,
	}
}

func (cache *Cache) SetReplaceAlgo(algo ReplacementAlgo) {
	cache.replacementAlgo = algo
}

func (cache *Cache) Put(key, value any) {
	if _, found := cache.data[key]; found {
		cache.data[key] = &cacheElemInfo{value, 0, false} // just update value
		return
	}

	if cache.capacity == cache.maxCapacity {
		cache.replacementAlgo.Replace(cache, key, value)
	} else {
		cache.capacity++
		cache.data[key] = &cacheElemInfo{value, 0, false}
	}
}

func (cache *Cache) Get(key any) (any, bool) {
	info := cache.data[key]
	if info == nil {
		return nil, false
	}

	info.used++

	return info.value, true
}

type LFUAlgo struct{}

func (algo LFUAlgo) Replace(cache *Cache, key any, value any) {
	fmt.Println("LFU IS HERE")
}

type RandomAlgo struct{}

func (algo RandomAlgo) Replace(cache *Cache, key any, value any) {
	fmt.Println("Random is here")
}

type SecondChanceAlgo struct{}

func (algo SecondChanceAlgo) Replace(cache *Cache, key any, value any) {
	fmt.Println("Second change algo")
}

func StrategyUsage() {
	cache := NewCache(LFUAlgo{})

	cache.Put(1, 1)
	cache.Put(2, 1)
	cache.Put(3, 1)
	cache.Put(4, 1)
	cache.Put(5, 1)
	cache.Put(6, 1)
	cache.Put(7, 1)
	cache.Put(8, 1)
	cache.Put(9, 1) // replacement here

	cache.SetReplaceAlgo(RandomAlgo{})
	cache.Put(10, 1) // replacement again
	// ...
}
