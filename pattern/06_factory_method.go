package pattern

import (
	"database/sql"
	"errors"
	"log"
)

/*
	Реализовать паттерн «фабричный метод».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Factory_method_pattern
*/

type Order struct {
	Id   int
	Name string
}

type OrderRepo interface {
	FindById(id int) (*Order, error)
	Insert(order *Order) error
}

var ErrNotImplemented = errors.New("not implemented")
var ErrNotFound = errors.New("not found")

// database

type OrderRepoImpl sql.DB

func (repo *OrderRepoImpl) FindById(id int) (*Order, error) {
	// select query here
	return nil, ErrNotImplemented
}

func (repo *OrderRepoImpl) Insert(order *Order) error {
	// insert query here
	return ErrNotImplemented
}

// mock

type OrderRepoMock map[int]*Order

func (repo *OrderRepoMock) FindById(id int) (*Order, error) {
	if val, ok := (*repo)[id]; !ok {
		return nil, ErrNotFound
	} else {
		return val, nil
	}
}

func (repo *OrderRepoMock) Insert(order *Order) error {
	(*repo)[order.Id] = order
	return nil
}

type Factory interface {
	NewOrderRepo() OrderRepo
}

type DBObjectFactory struct {
	// database options here
}

func (factory *DBObjectFactory) NewOrderRepo() OrderRepo {
	// initialize db somehow
	return &OrderRepoImpl{}
}

type MockCreator struct {
	// mb some options here
}

func (factory *MockCreator) NewOrderRepo() OrderRepo {
	// mb order initialization here
	return &OrderRepoMock{}
}

func User(factory Factory) {
	repo := factory.NewOrderRepo()

	if err := repo.Insert(&Order{1, "I am order"}); err != nil {
		log.Fatal(err)
	}

	if _, err := repo.FindById(1); err != nil {
		log.Fatal(err)
	}
}

func Usage() {
	// использование в нормальном случае
	User(&DBObjectFactory{})

	// использование при тестировании
	User(&MockCreator{})
}