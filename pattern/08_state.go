package pattern

import (
	"errors"
	"sync"
	"time"
)

/*
	Реализовать паттерн «состояние».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/State_pattern
*/

type StateConstant uint8

const (
	NotStarted = iota
	Pending
	Successful
	Failed
)

type State interface {
	Action(req *Request, expectedState StateConstant) error
}

type NotStartedState struct {
	mystate StateConstant
}

var ErrWrongState = errors.New("wrorng state")

func (state NotStartedState) Action(req *Request, expectedState StateConstant) error {
	if expectedState != state.mystate {
		return ErrWrongState
	}

	go func(request *Request) {
		req.state = PendingState{Pending}
		time.Sleep(100 * time.Millisecond)

		req.guard.Lock()
		defer req.guard.Unlock()
		req.data = "Executed"
	}(req)

	return nil
}

type PendingState struct {
	mystate StateConstant
}

var ErrPending = errors.New("pending")

func (state PendingState) Action(req *Request, expectedState StateConstant) error {
	if expectedState != state.mystate {
		return ErrWrongState
	}

	req.guard.RLock()
	defer req.guard.RUnlock()

	if req.data == "" {
		return ErrPending
	}

	req.state = SuccessfulState{Successful}
	return nil
}

type SuccessfulState struct {
	mystate StateConstant
}

func (state SuccessfulState) Action(req *Request, expectedState StateConstant) error {
	if expectedState != state.mystate {
		return ErrWrongState
	}

	return nil
}

type Request struct {
	guard sync.RWMutex
	state State
	data  string
	// request data
}

func NewRequest() *Request {
	return &Request{guard: sync.RWMutex{}, state: NotStartedState{NotStarted}}
}

func (req *Request) ExecuteRequest() error {
	return req.state.Action(req, NotStarted)
}

func (request *Request) GetData() (data string, err error) {
	err = request.state.Action(request, Successful)
	if err != nil {
		return
	}

	return request.data, nil
}
