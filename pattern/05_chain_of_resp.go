package pattern

import (
	"context"
	"net/http"
)

/*
	Реализовать паттерн «цепочка вызовов».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern
*/

// реализация на примере http обработчиков

// Filter - это функция, которая получает следующий обработчик (Handler) и возвращает новый обработчик.
type Filter func(handler http.Handler) http.Handler

type RequestHandler struct {
	handlers map[string]http.Handler
	filters  []Filter
}

func (rh *RequestHandler) AddHandler(uri string, handler http.Handler) {
	rh.handlers[uri] = handler
}

func (rh *RequestHandler) AddFilter(filter Filter) {
	rh.filters = append(rh.filters, filter)
}

func (handler *RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// поиск нужного обработчика
	function := handler.handlers[r.RequestURI]
	if function == nil {
		return
	}

	// создаем цепочку обработчиков
	for index := len(handler.filters) - 1; index >= 0; index-- {
		// каждый вызов filters[index](function) порождает новый объект типа Handler, в который передается следующий обработчик
		function = handler.filters[index](function)
	}

}

// примеры обработчиков

func CheckCustomHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if len(r.Header.Get("X-CUSTOM-HEADER")) == 0 {
			return
		}

		next.ServeHTTP(w, r)
	})
}

type UserInfo string

func (user UserInfo) IsEmpty() bool {
	return len(string(user)) == 0
}

var tokens = map[string]UserInfo{"token": "I am user"}

func AuthFilter(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		if len(token) == 0 {
			return
		}

		user := tokens[token]
		if user.IsEmpty() {
			return
		}

		ctx := context.WithValue(r.Context(), "user", user)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// финальный обработчик

func HelloHandler(w http.ResponseWriter, r *http.Request) {
	// some actions
	_, _ = w.Write([]byte("Hello"))
	w.Header().Set("Content-Type-", "text/plain")
}

// пример использования

func UsageExample() {
	handler := &RequestHandler{}

	handler.AddFilter(CheckCustomHeader)
	handler.AddFilter(AuthFilter)

	handler.AddHandler("/", http.HandlerFunc(HelloHandler))

	// sample
	//server := &http.Server{
	//	Handler: handler,
	// other options
	//}
	//

}
