package pattern

/*
	Реализовать паттерн «строитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Builder_pattern
*/

type Payment struct {
	Transaction  string `json:"transaction"`
	RequestId    string `json:"request_id"`
	Currency     string `json:"currency"`
	Provider     string `json:"provider"`
	Amount       uint   `json:"amount"`
	PaymentDt    uint64 `json:"payment_dt"`
	Bank         string `json:"bank"`
	DeliveryCost uint   `json:"delivery_cost"`
	GoodsTotal   uint   `json:"goods_total"`
	CustomFee    uint   `json:"custom_fee"`
}

type PaymentBuilder struct {
	payment *Payment
}

func NewBuilder() *PaymentBuilder {
	return &PaymentBuilder{new(Payment)}
}

func (builder *PaymentBuilder) Transaction(transaction string) *PaymentBuilder {
	builder.payment.Transaction = transaction
	return builder
}

func (builder *PaymentBuilder) RequestId(requestId string) *PaymentBuilder {
	builder.payment.RequestId = requestId
	return builder
}

func (builder *PaymentBuilder) Currency(currency string) *PaymentBuilder {
	builder.payment.Currency = currency
	return builder
}

func (builder *PaymentBuilder) Provider(provider string) *PaymentBuilder {
	builder.payment.Provider = provider
	return builder
}

func (builder *PaymentBuilder) Amount(amount uint) *PaymentBuilder {
	builder.payment.Amount = amount
	return builder
}

func (builder *PaymentBuilder) PaymentDt(paymentDt uint64) *PaymentBuilder {
	builder.payment.PaymentDt = paymentDt
	return builder
}

func (builder *PaymentBuilder) Bank(bank string) *PaymentBuilder {
	builder.payment.Bank = bank
	return builder
}

func (builder *PaymentBuilder) DeliveryCost(deliveryCost uint) *PaymentBuilder {
	builder.payment.DeliveryCost = deliveryCost
	return builder
}

func (builder *PaymentBuilder) GoodsTotal(goodsTotal uint) *PaymentBuilder {
	builder.payment.GoodsTotal = goodsTotal
	return builder
}

func (builder *PaymentBuilder) CustomFee(customFee uint) *PaymentBuilder {
	builder.payment.CustomFee = customFee
	return builder
}

func (builder *PaymentBuilder) Build() *Payment {
	return builder.payment
}
