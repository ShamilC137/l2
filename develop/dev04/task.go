package main

import (
	"fmt"
	"sort"
	"strings"
	"unsafe"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

// RuneInterface used for 'rune' sort
type RuneInterface []rune

func (slice RuneInterface) Len() int {
	return len(slice)
}

func (slice RuneInterface) Less(i, j int) bool {
	return slice[i] < slice[j]
}

func (slice RuneInterface) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func findAnagrams(values []string) map[string][]string {
	sorted := make(map[string][]string)

	for _, value := range values {
		lower := strings.ToLower(value)

		data := []rune(lower)
		sort.Sort(RuneInterface(data))
		str := *(*string)(unsafe.Pointer(&data))
		sorted[str] = append(sorted[str], lower)
	}

	result := make(map[string][]string, len(sorted))
	for _, value := range sorted {
		if len(value) < 2 {
			continue
		}

		key := value[0]
		sort.Strings(value)
		result[key] = append(result[key], value...)
	}
	return result
}

func main() {
	//'пятак', 'пятка' и 'тяпка'
	//'листок', 'слиток' и 'столик'
	source := []string{"пятак", "пятка", "столик", "тяпка", "слиток", "листок"}
	fmt.Println(findAnagrams(source))
}
