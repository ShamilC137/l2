package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"unsafe"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type cutter struct {
	delim     string
	fields    []int
	separated bool

	lines []string
}

func (c *cutter) cut() {
	scanner := bufio.NewScanner(os.Stdin)
	j := joiner{delim: []byte(c.delim)}
	all := !c.separated
	for scanner.Scan() {
		val := scanner.Text()
		split := strings.Split(val, c.delim)
		if len(split) == 1 {
			if all {
				c.lines = append(c.lines, val) // have no delim
			}
			continue
		}

		var line []string // reallocations, mb replace with joiner
		length := len(split)
		for _, index := range c.fields {
			if index < length {
				line = append(line, split[index])
			} else {
				line = append(line, "")
			}
		}

		c.lines = append(c.lines, j.join(line))
	}
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

type joiner struct {
	buffer []byte
	delim  []byte
}

// join returns mutable string, need at least two joiners
func (joiner joiner) join(data []string) string {
	nElems := len(data)
	length := (nElems - 1) * len(joiner.delim)
	for _, elem := range data {
		length += len(elem)
	}

	if cap(joiner.buffer) < length {
		newBuffer := make([]byte, length, length+(length>>2))
		copy(newBuffer, joiner.buffer)
		joiner.buffer = newBuffer
	}

	joiner.buffer = joiner.buffer[:0]
	for index, end := 0, nElems-1; index < end; index++ {
		elem := data[index]
		size := len(elem)
		// copy string data (in bytes) without creating new []byte (when you using []byte(string), you create new []byte)
		// 0x7fff0000 - max size
		if size != 0 {
			joiner.buffer = append(joiner.buffer, (*[0x7fff0000]byte)(unsafe.Pointer((*reflect.StringHeader)(unsafe.Pointer(&elem)).Data))[:size:size]...)
		}
		joiner.buffer = append(joiner.buffer, joiner.delim...)
	}

	size := len(data[nElems-1])
	if size != 0 {
		joiner.buffer = append(joiner.buffer, (*[0x7fff0000]byte)(unsafe.Pointer((*reflect.StringHeader)(unsafe.Pointer(&data[nElems-1])).Data))[:size:size]...)
	}

	return *(*string)(unsafe.Pointer(&joiner.buffer)) // mutable string
}

var flags = map[string]bool{"f": false, "d": false, "s": false}

func parseFields(value string, c *cutter) {
	data := strings.Split(value, ",")
	for _, elem := range data {
		val, err := strconv.Atoi(elem)
		handleError(err)
		for _, field := range c.fields {
			if field == val {
				continue
			}
		}

		c.fields = append(c.fields, val-1)
	}
}

func parseDelim(value string, c *cutter) {
	if len(value) != 1 {
		fmt.Println("Delimiter must be a single character")
		os.Exit(1)
	}

	c.delim = value
}

func main() {
	if len(os.Args) == 1 {
		fmt.Println("You must specify field!")
		os.Exit(1)
	}

	c := &cutter{}

	var handler func(string, *cutter)
	for _, arg := range os.Args[1:] {
		if handler != nil {
			handler(arg, c)
			handler = nil
			continue
		}

		if arg[0] == '-' {
			if len(arg) == 1 {
				fmt.Println("Flag value not specified (after -)")
				os.Exit(1)
			}

			set, found := flags[arg[1:]]
			if !found {
				fmt.Printf("Unknown flag: %s\n", arg)
				os.Exit(1)
			}

			if set {
				fmt.Printf("Flag already specified: %s", arg)
				os.Exit(1)
			}

			flags[arg[:1]] = true

			switch arg {
			case "-f":
				handler = parseFields
			case "-d":
				handler = parseDelim
			case "-s":
				c.separated = true
			}
		} else {
			fmt.Println("Only flags and their values are expected")
			os.Exit(1)
		}
	}

	if len(c.fields) == 0 {
		fmt.Println("You must specify field (of fields)")
		os.Exit(1)
	}

	if c.delim == "" {
		c.delim = "\t"
	}

	c.cut()

	for _, line := range c.lines {
		fmt.Println(line)
	}
}
