package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"unsafe"
)

/*
=== Утилита sort ===

Отсортировать строки (man sort)
Основное

Поддержать ключи

-k — указание колонки для сортировки
-n — сортировать по числовому значению
-r — сортировать в обратном порядке
-u — не выводить повторяющиеся строки

Дополнительное

Поддержать ключи

-M — сортировать по названию месяца
-b — игнорировать хвостовые пробелы
-c — проверять отсортированы ли данные
-h — сортировать по числовому значению с учётом суффиксов

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

// Правила сравнения:
// 1) при передаче -k флага, если требуемый столбец не существует, то сравнение будет производиться с пустой строкой
// 2) при передаче -n, -m флагов: не числа < чисел и месяцев
// 3) сравнение учитывает регистр (M < m)
// 4) сравнение учитывает знак (-1 < 1)
// 5) сравнение учитывает все переданные знаки (-a < a, так как "-" < "a")
// 6) при передаче -k флага учитываются только указанные столбцы
// 7) при одновременной передаче -n и -m флагов, учитывается -n
const (
	reversed uint8 = 1 << iota
	unique
	numeric
	month
	ignoreTailWhitespaces
	check
	suffix
)

type joiner struct {
	buffer []byte
}

// join returns mutable string, need at least two joiners
func (joiner *joiner) join(data []string) string {
	length := 0
	for _, elem := range data {
		length += len(elem)
	}

	if cap(joiner.buffer) < length {
		newBuffer := make([]byte, length, length+length>>2)
		copy(newBuffer, joiner.buffer)
		joiner.buffer = newBuffer
	}

	joiner.buffer = joiner.buffer[:0]
	for _, elem := range data {
		size := len(elem)
		if size == 0 {
			continue // unpacking nil slice -> error
		}
		// copy string data (in bytes) without creating new []byte (when you using []byte(string), you create new []byte)
		// 0x7fff0000 - max size
		joiner.buffer = append(joiner.buffer, (*[0x7fff0000]byte)(unsafe.Pointer((*reflect.StringHeader)(unsafe.Pointer(&elem)).Data))[:size:size]...)
	}

	return *(*string)(unsafe.Pointer(&joiner.buffer)) // mutable string
}

type sortConfiguration struct {
	props   uint8
	columns []int
}

var flags = map[string]uint8{"k": 0, "n": numeric, "r": reversed, "u": unique, "M": month, "b": ignoreTailWhitespaces, "c": check, "h": suffix}

func resolveComparator(conf sortConfiguration) comparator {
	switch {
	case conf.props&numeric == numeric:
		cmp := &numericComparator{}
		if len(conf.columns) != 0 {
			return &numericColumnComparator{numericComparator: cmp, columns: conf.columns}
		}

		return cmp
	case conf.props&month == month:
		cmp := &monthComparator{}
		if len(conf.columns) != 0 {
			return &monthColumnComparator{monthComparator: cmp, columns: conf.columns}
		}
		return cmp
	default:
		if len(conf.columns) == 0 {
			return &fullStringComparator{&joiner{}, &joiner{}}
		}
		return &columnsComparator{conf.columns}
	}
}

func main() {
	conf := sortConfiguration{}
	var file *os.File
	args := os.Args
	var nextNumber bool

	for _, arg := range args[1:] {
		if nextNumber {
			column, err := strconv.Atoi(arg)
			if err != nil {
				fmt.Printf("Integer value expected. %s\n", err)
				os.Exit(1)
			}

			conf.columns = append(conf.columns, column-1)
			nextNumber = false
			continue
		}

		if arg[0] == '-' {
			if len(arg) == 1 {
				fmt.Println("Flag expected (after -)")
				os.Exit(1)
			}

			flag, found := flags[arg[1:]]
			if !found {
				fmt.Printf("Unknown flag: %s\n", arg)
				os.Exit(1)
			}

			if flag == 0 {
				nextNumber = true
			} else {
				conf.props |= flag // enable flag
			}

			continue
		}

		if file != nil {
			fmt.Printf("Filename already present\n")
			os.Exit(1)
		}

		var err error
		if stat, err := os.Stat(arg); err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			if !stat.Mode().IsRegular() {
				fmt.Println("File must be a regular file!")
				os.Exit(1)
			}
		}

		file, err = os.Open(arg)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	if nextNumber {
		fmt.Println("Flag (-k) given but not specified")
		os.Exit(1)
	}

	if file == nil {
		fmt.Println("You must specify file")
	}

	sort := sorter{scanner: bufio.NewScanner(file), cmp: resolveComparator(conf), props: conf.props, uniqueLines: make(map[string]struct{})}
	if (conf.props & check) == check {
		index, sorted := sort.validate()
		if !sorted {
			fmt.Printf("Not sorted, disorder line: %d\n", index)
		} else {
			fmt.Println("Sorted")
		}
	} else {
		for _, value := range sort.sort() {
			fmt.Println(value)
		}
	}
}
