package main

import (
	"bufio"
	"strings"
)

const (
	initBufferSize = 64
)

type sorter struct {
	// for both validate and sort
	cmp           comparator
	scanner       *bufio.Scanner
	props         uint8
	nextGenerator func() (string, bool)
	uniqueLines   map[string]struct{}
}

func (sort *sorter) next() (string, bool) {
	if sort.scanner.Scan() {
		return sort.scanner.Text(), true
	}

	return "", false
}

func (sort *sorter) nextTrimmed() (string, bool) {
	next, ok := sort.next()
	if !ok {
		return "", false
	}

	return strings.TrimRight(next, " "), true
}

func (sort *sorter) nextUnique() (string, bool) {
	generator := sort.next
	if sort.props&ignoreTailWhitespaces == ignoreTailWhitespaces {
		generator = sort.nextTrimmed
	}

	for {
		value, ok := generator()
		if !ok {
			return "", false
		}

		if _, found := sort.uniqueLines[value]; found {
			continue
		}

		sort.uniqueLines[value] = struct{}{}
		return value, true
	}
}

// must be called by validate and sort only
func (sort *sorter) getNext() (string, bool) {
	if sort.nextGenerator != nil {
		return sort.nextGenerator()
	}

	sort.nextGenerator = sort.next
	switch {
	case (sort.props & unique) == unique:
		sort.nextGenerator = sort.nextUnique
	case (sort.props & ignoreTailWhitespaces) == ignoreTailWhitespaces:
		sort.nextGenerator = sort.nextTrimmed
	}

	return sort.nextGenerator()
}

func (sort *sorter) validate() (int, bool) {
	value, ok := sort.getNext()
	if !ok {
		return 0, true
	}

	prev := tokenize(value)
	line := 1
	if sort.props&reversed == reversed {
		for value, ok := sort.getNext(); ok; value, ok = sort.getNext() {
			current := tokenize(value)
			if sort.cmp.compare(prev, current) > 0 {
				return line, false
			}
			line++
		}
	} else {
		for value, ok := sort.getNext(); ok; value, ok = sort.getNext() {
			current := tokenize(value)
			if sort.cmp.compare(prev, current) < 0 {
				return line, false
			}
			line++
		}
	}

	return 0, true
}

func (sort *sorter) sort() []string {
	searcher := binarySearch
	if sort.props&reversed == reversed {
		searcher = reversedBinarySearch
	}

	lines := make([]string, 0, initBufferSize)
	tokens := make([][]string, 0, initBufferSize)

	for value, ok := sort.getNext(); ok; value, ok = sort.getNext() {
		current := tokenize(value)
		pos := searcher(tokens, current, sort.cmp)
		tokens = insert(tokens, current, pos)
		lines = insert(lines, value, pos)
	}

	return lines
}

func reversedBinarySearch(slice [][]string, value []string, cmp comparator) int {
	first := 0
	last := len(slice) - 1
	for first <= last {
		pos := (first + last) >> 1
		comparison := cmp.compare(slice[pos], value)
		switch {
		case comparison == 0:
			return pos
		case comparison < 0:
			last = pos - 1
		case comparison > 0:
			first = pos + 1
		}
	}

	return first
}

func binarySearch(slice [][]string, value []string, cmp comparator) int {
	first := 0
	last := len(slice) - 1
	for first <= last {
		pos := (first + last) >> 1
		comparison := cmp.compare(slice[pos], value)
		switch {
		case comparison == 0:
			return pos
		case comparison < 0:
			first = pos + 1
		case comparison > 0:
			last = pos - 1
		}
	}
	return first
}

func insert[T any](slice []T, value T, pos int) []T {
	length := len(slice)
	slice = append(slice, value)
	copy(slice[pos+1:], slice[pos:length])
	slice[pos] = value
	return slice
}

func tokenize(value string) []string {
	return strings.Fields(value)
}
