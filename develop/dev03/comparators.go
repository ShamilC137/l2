package main

import (
	"strconv"
	"strings"
)

var months map[string]int = map[string]int{"jan": 1, "feb": 2, "mar": 3, "apr": 4, "may": 5, "jun": 6, "jul": 7, "aug": 8, "sep": 9, "oct": 10, "nov": 11, "dec": 12}

type comparator interface {
	compare(lhs, rhs []string) int
}

type fullStringComparator struct {
	lhsJoiner *joiner
	rhsJoiner *joiner
}

func (comp *fullStringComparator) compare(lhs, rhs []string) int {
	first := comp.lhsJoiner.join(lhs)
	last := comp.rhsJoiner.join(rhs)
	switch {
	case first < last:
		return -1
	case first == last:
		return 0
	default:
		return 1
	}
}

type columnsComparator struct {
	columns []int
}

func stringCompare(first, second string) int {
	switch {
	case first < second:
		return -1
	case first == second:
		return 0
	default:
		return 1
	}
}

func (comp *columnsComparator) compare(lhs, rhs []string) int {
	lhsLength := len(lhs)
	rhsLength := len(rhs)

	for _, column := range comp.columns {
		first := ""
		if lhsLength > column {
			first = lhs[column]
		}

		second := ""
		if rhsLength > column {
			second = rhs[column]
		}

		if first == second {
			continue
		}

		if first < second {
			return -1
		}

		if first > second {
			return 1
		}
	}

	return 0
}

type numericComparator struct{}

func (cmp *numericComparator) numericCompare(first, second string) (int, bool) {
	firstVal, firstErr := strconv.Atoi(first)
	secondVal, secondErr := strconv.Atoi(second)

	if firstErr == nil {
		if secondErr == nil {
			return firstVal - secondVal, false
		}

		return 1, false
	} else if secondErr == nil {
		return -1, false
	}

	return stringCompare(first, second), true
}

func (cmp *numericComparator) compare(lhs, rhs []string) int {
	return doubleCompare(lhs, rhs, cmp.numericCompare)
}

type numericColumnComparator struct {
	*numericComparator
	columns []int
}

func (cmp *numericColumnComparator) compare(lhs, rhs []string) int {
	return doubleColumnCompare(lhs, rhs, cmp.columns, cmp.numericCompare)
}

type monthComparator struct{}

func (cmp *monthComparator) monthCompare(lhs, rhs string) (int, bool) {
	firstVal, firstFound := months[strings.ToLower(lhs)]
	secondVal, secondFound := months[strings.ToLower(rhs)]

	if firstFound {
		if secondFound {
			return firstVal - secondVal, false
		}
		return 1, false
	} else if secondFound {
		return -1, false
	}

	return stringCompare(lhs, rhs), true
}

func (cmp *monthComparator) compare(lhs, rhs []string) int {
	return doubleCompare(lhs, rhs, cmp.monthCompare)
}

type monthColumnComparator struct {
	*monthComparator
	columns []int
}

func (cmp *monthColumnComparator) compare(lhs, rhs []string) int {
	return doubleColumnCompare(lhs, rhs, cmp.columns, cmp.monthCompare)
}

func doubleCompare(lhs, rhs []string, firstCmp func(string, string) (int, bool)) int {
	lhsLength := len(lhs)
	rhsLength := len(rhs)

	min := lhsLength
	if min > rhsLength {
		min = rhsLength
	}

	var nextStrings bool
	for index := 0; index < min; index++ {
		var comparison int

		if nextStrings {
			comparison = stringCompare(lhs[index], rhs[index])
		} else {
			comparison, nextStrings = firstCmp(lhs[index], rhs[index])
		}

		if comparison == 0 {
			continue
		}

		return comparison
	}

	if lhsLength == rhsLength {
		return 0
	}

	if lhsLength < rhsLength {
		return stringCompare("", rhs[lhsLength])
	}

	return stringCompare(lhs[rhsLength], "")
}

func doubleColumnCompare(lhs, rhs []string, columns []int, firstCmp func(string, string) (int, bool)) int {
	lhsLength := len(lhs)
	rhsLength := len(rhs)

	var nextStrings bool
	for _, column := range columns {
		var first string
		if lhsLength > column {
			first = lhs[column]
		}

		var second string
		if rhsLength > column {
			second = rhs[column]
		}

		var comparison int
		if nextStrings {
			comparison = stringCompare(first, second)
		} else {
			comparison, nextStrings = firstCmp(first, second)
		}

		if comparison == 0 {
			continue
		}

		return comparison
	}

	return 0
}
