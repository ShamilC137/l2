package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func TestSort(t *testing.T) {
	testCases := []struct {
		name             string
		expectedFileName string
		input            []string
	}{
		{
			name:             "first",
			expectedFileName: ".\\test_results\\first",
			input:            []string{"file", "-b"},
		},
		{
			name:             "second",
			expectedFileName: ".\\test_results\\second",
			input:            []string{"-k", "1", "file", "-k", "2", "-b"},
		},
		{
			name:             "third",
			expectedFileName: ".\\test_results\\third",
			input:            []string{"file", "-n", "-u", "-k", "2", "-k", "1", "-b"},
		},
		{
			name:             "fourth",
			expectedFileName: ".\\test_results\\fourth",
			input:            []string{"file", "-M", "-u", "-b"},
		},
		{
			name:             "fifth",
			expectedFileName: "",
			input:            []string{"file", "-k"},
		},
	}

	binName := ".\\cli.exe"
	if err := exec.Command("go", "build", "-o", binName, ".").Run(); err != nil {
		t.Fatal(err)
	}
	defer os.Remove(binName)

	buffer := bytes.NewBuffer([]byte{})
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			cmd := exec.Command(binName, tc.input...)
			cmd.Stdout = buffer
			if err := cmd.Run(); err != nil {
				if len(tc.expectedFileName) == 0 { // error expected
					return
				}

				t.Fatal(err)
			}

			data, err := ioutil.ReadFile(tc.expectedFileName)
			if err != nil {
				t.Fatal(err)
			}

			if strings.Replace(string(data), "\r", "", -1) != buffer.String() {
				t.Error("Different strings")
			}
			buffer.Reset()
		})
	}
}
