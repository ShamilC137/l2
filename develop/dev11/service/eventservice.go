package service

import (
	"Task11/datasource"
	"Task11/datasource/model"
	"Task11/datasource/repo"
	"Task11/utility"
	"context"
	"errors"
	"time"
)

// ErrBrokenTimeInterval is returned when given time interval is broken (i.e. begin >= end)
var ErrBrokenTimeInterval = errors.New("begin must be < end")

// EventService contains validation logic for repo's parameters
type EventService struct {
	*repo.EventRepo
}

// NewEventService returns new EventService object
func NewEventService() (*EventService, error) {
	db, err := datasource.GetDatasourceInstance("") // must be configurated
	if err != nil {
		return nil, err
	}

	eventRepo, err := repo.GetEventRepo(db)
	if err != nil {
		return nil, err
	}

	return &EventService{eventRepo}, nil
}

// Update extracts value with the specified id from the datasource and merges given and extracted values, then calls repo's Update
func (eventService *EventService) Update(ctx context.Context, event *model.Event) error {
	val, err := eventService.FindByID(ctx, event.ID)
	if err != nil {
		return err
	}

	_, err = utility.Merge(event, val)
	if err != nil {
		return err
	}

	return eventService.EventRepo.Update(ctx, event)
}

// FindInRange validates time interval and calls
func (eventService *EventService) FindInRange(ctx context.Context, begin time.Time, end time.Time) ([]*model.Event, error) {
	if !begin.Before(end) {
		return nil, ErrBrokenTimeInterval
	}

	return eventService.EventRepo.FindInRange(ctx, begin, end)
}
