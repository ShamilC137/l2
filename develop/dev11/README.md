# Структура пакетов
- [datasource](datasource) - содержит все классы, необходимые для работы с БД
- [model](datasource/model) - описывает объекты БД
- [repo](datasource/repo) - предоставляет объекты для работы с отношениями БД
- [dto](dto) - описывает DTO (все запросы парсят сложные объекты при помощи них)
- [server](server) - содержит сущности для создания и работы с сервером.   
P.S. изначально я не правильно понял задание, из-за чего не использовал кастомные роутеры (например, gorilla/mux, который делает то же самое), 
а написал свой. Понял ошибку я довольно поздно, поэтому удалять мне стало это жаль (Validator написан по той же причине, 
можно заменить на go-playground/validator)
- [service](service) - содержит классы, оборачивающие репозитории и добавляющие доп. логику к методам репозитория (если необходимо)
- [utility](utility) - содержит вспомогательные функции

``/create_event``, POST body example: creatorUserId=1&name=sample&date=2023-01-01T00:00:00Z,
all required  
``/update_event``, POST body example: id=1&modifierUserId=1&name=sample&date=2023-01-01T00:00:00+03:00,
required: id, modifierUserId
``/delete_event``, POST body example: id=1, required  
``/events_for_day``, ``/events_for_week``, ``/events_for_month`` GET query: begin=2023-01-01, required


> Date format для GET запросов: YYYY-MM-DD, интерпретируется как UTC (т.е. поиск начинается в 00:00:00 UTC и кончается в 23:59:59 UTC)