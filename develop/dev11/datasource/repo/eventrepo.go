package repo

import (
	"Task11/datasource/model"
	"Task11/utility"
	"context"
	"database/sql"
	"time"
)

// EventRepo is a object that allows to work with event's datasource object
type EventRepo struct {
	*baseRepo
}

var eventRepoInstance *EventRepo

// GetEventRepo returns EventRepo instance. Source can be nil for all calls except the first one
func GetEventRepo(source *sql.DB) (*EventRepo, error) {
	if eventRepoInstance != nil {
		return eventRepoInstance, nil
	}

	repo := &EventRepo{newBaseRepo(source)}

	_, err := repo.source.Exec("CREATE TABLE IF NOT EXISTS events (id serial primary key, " +
		"creator_user_id int not null references users, " +
		"modifier_user_id int not null references users, " +
		"\"name\" varchar(128), " +
		"\"date\" timestamp with time zone not null)")
	if err != nil {
		return nil, err
	}

	err = repo.createAndPutPreparedStatement("EventRepo.Create", "INSERT INTO events(creator_user_id, modifier_user_id, \"name\", \"date\") VALUES ($1, $2, $3, $4) RETURNING id")
	if err != nil {
		return nil, err
	}

	err = repo.createAndPutPreparedStatement("EventRepo.Update", "UPDATE events SET modifier_user_id = $1, \"name\" = $2, \"date\" = $3 WHERE id = $4")
	if err != nil {
		utility.LogFatal(repo.CloseRepo())
		return nil, err
	}

	err = repo.createAndPutPreparedStatement("EventRepo.FindInRangeFindBetween", "SELECT id, creator_user_id, modifier_user_id, \"name\", \"date\" "+
		"FROM events WHERE \"date\" >= $1 AND \"date\" < $2")
	if err != nil {
		utility.LogFatal(repo.CloseRepo())
		return nil, err
	}

	err = repo.createAndPutPreparedStatement("EventRepo.FindByID", "SELECT creator_user_id, modifier_user_id, \"name\", \"date\" FROM events WHERE id = $1")
	if err != nil {
		utility.LogFatal(repo.CloseRepo())
		return nil, err
	}

	err = repo.createAndPutPreparedStatement("EventRepo.Delete", "DELETE FROM events WHERE id = $1")
	if err != nil {
		utility.LogFatal(repo.CloseRepo())
		return nil, err
	}

	eventRepoInstance = repo
	return repo, nil
}

// Create creates new event. Updates given event (sets id)
func (repo *EventRepo) Create(ctx context.Context, event *model.Event) error {
	stmt, _ := repo.getStatement("EventRepo.Create")
	err := stmt.QueryRowContext(ctx, event.CreatorUserID, event.ModifierUserID, event.Name, event.Date).Scan(&event.ID)
	if err != nil {
		return err
	}

	return nil
}

// Update updates event with the specified ID. Updates given event. If fields of event are default, replaces it with
// fields extracted from database
func (repo *EventRepo) Update(ctx context.Context, event *model.Event) error {
	stmt, _ := repo.getStatement("EventRepo.Update")
	res, err := stmt.ExecContext(ctx, event.ModifierUserID, event.Name, event.Date, event.ID)
	if err != nil {
		return err
	}

	nrows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if nrows == 0 {
		return ErrNotFound
	}

	// should I update the source value?
	return nil
}

// FindInRange searches all record which date in range [begin; end)
func (repo *EventRepo) FindInRange(ctx context.Context, begin, end time.Time) ([]*model.Event, error) {
	stmt, _ := repo.getStatement("EventRepo.FindInRangeFindBetween")
	rows, err := stmt.QueryContext(ctx, begin, end)
	if err != nil {
		return nil, err
	}

	defer func() {
		utility.LogFatal(rows.Close())
	}()

	result := make([]*model.Event, 0)
	for rows.Next() {
		value := &model.Event{}
		err = rows.Scan(&value.ID, &value.CreatorUserID, &value.ModifierUserID, &value.Name, &value.Date)
		if err != nil {
			return nil, err
		}

		result = append(result, value)
	}

	return result, nil
}

// FindByID searches event with the specified id. Returns ErrNotFound if such event does not exist
func (repo *EventRepo) FindByID(ctx context.Context, id int) (*model.Event, error) {
	stmt, _ := repo.getStatement("EventRepo.FindByID")

	var name string
	var creatorUserID, modifierUserID int
	var date time.Time
	err := stmt.QueryRowContext(ctx, id).Scan(&creatorUserID, &modifierUserID, &name, &date)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}

		return nil, err
	}

	return &model.Event{ID: id, CreatorUserID: creatorUserID, ModifierUserID: modifierUserID, Name: name, Date: date}, nil
}

// Delete removes event with the specified ID. Return ErrNotFound if event does not exist
func (repo *EventRepo) Delete(ctx context.Context, id int) error {
	stmt, _ := repo.getStatement("EventRepo.Delete")

	res, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	nRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if nRows == 0 {
		return ErrNotFound
	}

	return nil
}
