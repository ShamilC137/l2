package repo

import (
	"database/sql"
	"errors"
	"sync"
)

var errStmtAlreadyExists = errors.New("statement with given key already exists")

type baseRepo struct {
	source *sql.DB
	cache  *sync.Map
}

func newBaseRepo(source *sql.DB) *baseRepo {
	return &baseRepo{source, &sync.Map{}}
}

func (repo *baseRepo) prepareStatement(query string) (stmt *sql.Stmt, err error) {
	stmt, err = repo.source.Prepare(query)
	if err != nil {
		return
	}

	return
}

func (repo *baseRepo) getStatement(key any) (*sql.Stmt, bool) {
	val, ok := repo.cache.Load(key)
	if !ok {
		return nil, false
	}

	return val.(*sql.Stmt), true
}

func (repo *baseRepo) putStatement(key any, stmt *sql.Stmt) error {
	if _, found := repo.cache.Load(key); found {
		return errStmtAlreadyExists
	}

	repo.cache.Store(key, stmt)

	return nil
}

func (repo *baseRepo) CloseRepo() error {
	var err error
	repo.cache.Range(func(_, value any) bool {
		stmt := value.(*sql.Stmt)
		err = stmt.Close()
		if err != nil {
			return false
		}
		return true
	})

	return err
}

func (repo *baseRepo) createAndPutPreparedStatement(key any, query string) error {
	if _, found := repo.cache.Load(key); found {
		return errStmtAlreadyExists
	}

	stmt, err := repo.prepareStatement(query)
	if err != nil {
		return err
	}

	repo.cache.Store(key, stmt)

	return nil
}
