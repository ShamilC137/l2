package repo

import "errors"

// ErrNotFound is returned when repo does not contain specified value
var ErrNotFound = errors.New("not found")
