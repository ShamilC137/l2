package repo

// should be inspected (out of usage)

import (
	"Task11/datasource/model"
	"context"
	"database/sql"
)

var userRepoInstance *UserRepo

// UserRepo represent object which allows to work with users in datasource
type UserRepo struct {
	*baseRepo
}

// GetUserRepo returns instance of UserRepo. Source can be nil for all calls except first successive one
func GetUserRepo(source *sql.DB) (*UserRepo, error) {
	if userRepoInstance != nil {
		return userRepoInstance, nil
	}

	repo := &UserRepo{newBaseRepo(source)}

	_, err := repo.source.Exec("CREATE TABLE IF NOT EXISTS users (id serial primary key, login varchar(128) not null)")
	if err != nil {
		return nil, err
	}

	if err = repo.createAndPutPreparedStatement("UserRepo.FindByID", "SELECT login FROM users WHERE id = $1"); err != nil {
		return nil, err
	}

	userRepoInstance = repo

	return repo, nil
}

// FindByID returns user with the specified ID. If such uses does not exist return ErrNotFound
func (repo *UserRepo) FindByID(ctx context.Context, id int32) (*model.User, error) {
	stmt, _ := repo.getStatement("UserRepo.FindByID")

	var login string
	err := stmt.QueryRowContext(ctx, id).Scan(&login)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}

		return nil, err
	}

	return &model.User{ID: id, Login: login}, nil
}
