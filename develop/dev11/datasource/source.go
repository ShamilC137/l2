package datasource

import (
	"Task11/utility"
	"database/sql"
	"errors"
	"fmt"
	"log"

	// here imports for load all supported DB drivers
	_ "github.com/lib/pq"
)

// Configuration is used to configure datasource
type Configuration struct {
	Driver   string // driver name (postgres, for example)
	Host     string
	Name     string
	Port     uint16
	User     string
	Password string
	Sslmode  string
}

// ErrConfigurationEmpty is returned when nil configuration is passed for the first call
var ErrConfigurationEmpty = errors.New("configuration path cannot be empty for first call")

// singleton
var instance *sql.DB = nil

// GetDatasourceInstance returns instance of database object. Configuration could be nil after first call.
// If database already created, returns created object otherwise creates it
func GetDatasourceInstance(key string) (*sql.DB, error) {
	if instance != nil {
		return instance, nil
	}

	if len(key) == 0 {
		return nil, ErrConfigurationEmpty
	}

	config, err := utility.ReadConfiguration[Configuration](key)

	log.Println("Trying to connect to the database")
	db, err := sql.Open(config.Driver, fmt.Sprintf("host=%s dbname=%s port=%d user=%s password=%s sslmode=%s",
		config.Host, config.Name, config.Port, config.User, config.Password, config.Sslmode))
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		if err := db.Close(); err != nil {
			return nil, err
		}
		return nil, err
	}

	log.Println("Connected to the database")

	instance = db
	return db, nil
}

// CloseDatasource closes DB object (by calling sql.DB.Close if instance not nil)
func CloseDatasource() error {
	if instance != nil {
		return instance.Close()
	}

	return nil
}
