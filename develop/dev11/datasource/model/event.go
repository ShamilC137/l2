package model

import (
	"time"
)

// Event is representing event object.
// date format: YYYY-MM-DDThh:mm:ssZ
type Event struct {
	Name           string
	ID             int
	CreatorUserID  int
	ModifierUserID int
	Date           time.Time
}
