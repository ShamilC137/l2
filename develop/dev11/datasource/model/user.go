package model

// User represent user object
type User struct {
	Login string
	ID    int32
}
