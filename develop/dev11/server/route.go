package server

import "net/http"

// HTTPMethod represents http method
type HTTPMethod uint16

// GET HTTP method
const (
	GET HTTPMethod = 1 << iota
	PUT
	POST
	OPTIONS
	DELETE
	HEAD
	PATCH
	CONNECT
	TRACE
)

var (
	methods = map[string]HTTPMethod{"GET": GET, "PUT": PUT, "POST": POST, "OPTIONS": OPTIONS, "DELETE": DELETE,
		"HEAD": HEAD, "PATCH": PATCH, "CONNECT": CONNECT, "TRACE": TRACE}
)

// Route stores information to match a request
type Route struct {
	consumes    map[string]struct{}
	middlewares []MiddlewareFunc
	handler     http.Handler
	allowed     HTTPMethod
}

// NewRoute returns new Route object
func NewRoute(handler http.Handler) *Route {
	return &Route{handler: handler, consumes: make(map[string]struct{})}
}

// AddMiddleware adds middleware to the route
func (route *Route) AddMiddleware(middlewareFunc MiddlewareFunc) *Route {
	route.middlewares = append(route.middlewares, middlewareFunc)
	return route
}

// AddConsumedType adds consumed type to the route. If consumed types container is empty, matches all
func (route *Route) AddConsumedType(contentType string) *Route {
	route.consumes[contentType] = struct{}{}
	return route
}

// AddMethod adds allowed method to the route. If there is no allowed methods, allows all
func (route *Route) AddMethod(method HTTPMethod) *Route {
	route.allowed |= method
	return route
}
