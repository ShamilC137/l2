package server

import (
	"Task11/datasource/model"
	"Task11/datasource/repo"
	"Task11/service"
	"Task11/utility"
	"fmt"
	"net/http"
	"time"
)

// EventController handles all request associated with events
type EventController struct {
	eventService *service.EventService
}

// NewEventController returns new EventController object
func NewEventController() (*EventController, error) {
	eventService, err := service.NewEventService()
	if err != nil {
		return nil, err
	}

	return &EventController{eventService: eventService}, nil
}

// HandleCreateEvent a handles request to create an event
func (controller *EventController) HandleCreateEvent(writer http.ResponseWriter, request *http.Request) {
	event := request.Context().Value(modelKey).(*model.Event)

	event.ModifierUserID = event.CreatorUserID
	err := controller.eventService.Create(request.Context(), event)
	if err != nil {
		utility.WriteErrorAsJSON(err, writer, 503)
		return
	}

	utility.WriteAsJSON(event, writer, http.StatusCreated)
}

// HandleUpdateEvent handles a request to update an event
func (controller *EventController) HandleUpdateEvent(writer http.ResponseWriter, request *http.Request) {
	event := request.Context().Value(modelKey).(*model.Event)

	err := controller.eventService.Update(request.Context(), event)
	if err != nil {
		statusCode := 400
		if err != repo.ErrNotFound {
			statusCode = 503
		}
		
		utility.WriteErrorAsJSON(err, writer, statusCode)
		return
	}

	utility.WriteAsJSON(event, writer, http.StatusOK)
}

// HandleDeleteEvent handles a request to delete an event
func (controller *EventController) HandleDeleteEvent(writer http.ResponseWriter, request *http.Request) {
	id := request.Context().Value(idKey).(int)

	err := controller.eventService.Delete(request.Context(), id)
	if err != nil {
		statusCode := 400
		if err != repo.ErrNotFound {
			statusCode = 503
		}

		utility.WriteErrorAsJSON(err, writer, statusCode)
		return
	}

	writer.WriteHeader(204)
}

func (controller *EventController) findEvents(begin, end time.Time, writer http.ResponseWriter, request *http.Request) {
	events, err := controller.eventService.FindInRange(request.Context(), begin, end)
	if err != nil {
		utility.WriteErrorAsJSON(err, writer, 503)
		return
	}

	utility.WriteAsJSON(events, writer, 200)
}

// HandleEventsForDay handles a request to get all events on the specified day (date format: YYYY-MM-DD, UTC time, e.g. 2000-01-01 -> 2000-01-01T00:00:00Z)
func (controller *EventController) HandleEventsForDay(writer http.ResponseWriter, request *http.Request) {
	date := request.Context().Value(beginKey).(time.Time)
	controller.findEvents(date, date.Add(time.Hour*24), writer, request)
}

// HandleEventsForWeek handles a request to get all events on the specified week (date format: YYYY-MM-DD, UTC time, e.g. 2000-01-01 -> 2000-01-01T00:00:00Z)
func (controller *EventController) HandleEventsForWeek(writer http.ResponseWriter, request *http.Request) {
	date := request.Context().Value(beginKey).(time.Time)
	controller.findEvents(date, date.AddDate(0, 0, 7), writer, request)
}

// HandleEventsForMonth handles a request to get all events on the specified month (date format: YYYY-MM-DD, UTC time, e.g. 2000-01-01 -> 2000-01-01T00:00:00Z)
func (controller *EventController) HandleEventsForMonth(writer http.ResponseWriter, request *http.Request) {
	date := request.Context().Value(beginKey).(time.Time)
	controller.findEvents(date, date.AddDate(0, 1, 0), writer, request)
}

// EventCreationError contains information about which field failed to initialize and why
type EventCreationError struct {
	field   string
	message string
}

func (err *EventCreationError) Error() string {
	return fmt.Sprintf("cannot read property '%s'. Problem: '%s'", err.field, err.message)
}
