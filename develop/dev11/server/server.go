package server

import (
	"Task11/datasource/model"
	"Task11/dto"
	"Task11/utility"
	"context"
	"errors"
	"fmt"
	"github.com/gorilla/schema"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	shutdownTimeout = 5 * time.Second
)

type contextKey uint8

const (
	modelKey contextKey = iota
	idKey
	beginKey
)

var (
	modelDecoder *schema.Decoder
)

func init() {
	modelDecoder = schema.NewDecoder()
}

// ErrParseFormFailed is returned when http.Request.ParseForm fails
var ErrParseFormFailed = errors.New("cannot parse form")

// Configuration stores server settings
type Configuration struct {
	IP           string
	Port         int
	WriteTimeout time.Duration
	ReadTimeout  time.Duration
}

// SetupRouter returns new Router with all routes
func SetupRouter() *Router {
	router := NewRouter()
	router.AddMiddleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Printf("Processing for %s request\n", r.URL.Path)
			t1 := time.Now()
			w.Header().Set("Content-Type", "application/json")
			next.ServeHTTP(w, r)
			t2 := time.Now()
			log.Printf("Processing finished for %s\n", t2.Sub(t1))
		})
	})

	router.AddMiddleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm() // PostForm and Form
			if err != nil {
				utility.WriteErrorAsJSON(ErrParseFormFailed, w, 500)
				return
			}
			next.ServeHTTP(w, r)
		})
	})

	eventController, err := NewEventController()
	utility.LogFatal(err)

	//FIXME: subrouters
	router.AddHandler("/create_event", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleCreateEvent(w, r)
	})).AddMethod(POST).AddConsumedType("application/x-www-form-urlencoded").AddMiddleware(validateAndPut[dto.CreateEventDto, model.Event])

	router.AddHandler("/update_event", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleUpdateEvent(w, r)
	})).AddMethod(POST).AddConsumedType("application/x-www-form-urlencoded").AddMiddleware(validateAndPut[dto.UpdateEventDto, model.Event])

	router.AddHandler("/delete_event", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleDeleteEvent(w, r)
	})).AddMethod(POST).AddConsumedType("application/x-www-form-urlencoded").AddMiddleware(parseId)

	router.AddHandler("/events_for_day", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleEventsForDay(w, r)
	})).AddMethod(GET).AddMiddleware(parseBegin)

	router.AddHandler("/events_for_week", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleEventsForWeek(w, r)
	})).AddMethod(GET).AddMiddleware(parseBegin)

	router.AddHandler("/events_for_month", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		eventController.HandleEventsForMonth(w, r)
	})).AddMethod(GET).AddMiddleware(parseBegin)

	return router
}

func validateAndPut[Dto any, Model any](next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		object, httpError := utility.ExtractValue[Dto, Model](modelDecoder, r.PostForm)
		if httpError != nil {
			utility.WriteErrorAsJSON(httpError, w, httpError.Code)
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), modelKey, object)))
	})
}

func parseBegin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		data := request.Form["begin"]
		if len(data) != 1 {
			utility.WriteErrorAsJSON(utility.NewInvalidArgumentError("begin", "missed"), writer, 400)
			return
		}

		date, err := time.ParseInLocation("2006-01-02", data[0], time.UTC)
		if err != nil {
			utility.WriteErrorAsJSON(utility.NewInvalidArgumentError("begin", err.Error()), writer, 400) // seems that its wrong input
			return
		}

		ctx := context.WithValue(request.Context(), beginKey, date)
		next.ServeHTTP(writer, request.WithContext(ctx))
	})
}

func parseId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		idData := request.PostForm["id"]
		if len(idData) != 1 {
			utility.WriteErrorAsJSON(utility.NewInvalidArgumentError("id", "missed or too much arguments (expected 1)"), writer, 400)
			return
		}

		id, err := strconv.Atoi(idData[0])
		if err != nil {
			utility.WriteErrorAsJSON(utility.NewInvalidArgumentError("id", err.Error()), writer, 400)
			return
		}

		ctx := context.WithValue(request.Context(), idKey, id)

		next.ServeHTTP(writer, request.WithContext(ctx))
	})
}

// NewHTTPServer returns configured http server
func NewHTTPServer() (*http.Server, error) {
	config, err := utility.ReadConfiguration[Configuration]("server")
	if err != nil {
		return nil, err
	}

	return &http.Server{Handler: SetupRouter(),
		Addr:         fmt.Sprintf("%s:%d", config.IP, config.Port),
		WriteTimeout: config.WriteTimeout,
		ReadTimeout:  config.ReadTimeout}, nil
}

// StartServe accepts connection until given context stopped (i.e. <-stop.Done())
func StartServe(stop context.Context, server *http.Server) error {
	var serverError error
	go func() {
		log.Println("The server is started")
		serverError = server.ListenAndServe()
	}()

	<-stop.Done()

	log.Println("Starting the server shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		return err
	}

	if serverError != nil && serverError != http.ErrServerClosed {
		return serverError
	}

	log.Println("The server is down")
	return nil
}
