package server

import (
	"errors"
	"net/http"
	"path"
)

// Could be replaced with https://github.com/gorilla/mux

// ErrMethodNotAllowed is the returned when route exists but the method is not contained in allowed methods
var ErrMethodNotAllowed = errors.New("method not allowed")

// ErrHandlerNotFound is returned when the route with the specified url does not exist
var ErrHandlerNotFound = errors.New("handler not found")

// ErrUnsupportedMediaType is returned when the route exists but the media type is not contained in the consumed types container
var ErrUnsupportedMediaType = errors.New("unsupported media type")

// MiddlewareFunc represent middleware function (i.e. 'request interceptor')
type MiddlewareFunc func(http.Handler) http.Handler

// Router stores information about routes and global middlewares (i.e. middlewares for all handlers)
// It's unsafe to change router/route after server start
type Router struct {
	routes      map[string]*Route
	middlewares []MiddlewareFunc
}

// NewRouter returns new Router
func NewRouter() *Router {
	return &Router{routes: make(map[string]*Route)}
}

func cleanPath(uri string) string {
	if uri == "" {
		return "/"
	}
	if uri[0] != '/' {
		uri = "/" + uri
	}

	result := path.Clean(uri)
	// reason: when you call clean for url /hello/ it will trim last / (result - /hello)
	// to avoid this we need to add last '/' manually
	if uri[len(uri)-1] == '/' && result != "/" {
		return result + "/"
	}
	return result
}

// AddHandler adds handler to handlers container
func (router *Router) AddHandler(url string, handler http.Handler) *Route {
	route := NewRoute(handler)
	router.routes[cleanPath(url)] = route
	return route
}

func (router *Router) match(path string, request *http.Request) (http.Handler, error) {
	route := router.routes[path]
	if route == nil {
		return nil, ErrHandlerNotFound
	}

	if route.allowed != 0 {
		if method := methods[request.Method]; method&route.allowed == 0 {
			return nil, ErrMethodNotAllowed
		}
	}

	if len(route.consumes) != 0 {
		if _, found := route.consumes[request.Header.Get("Content-Type")]; !found {
			return nil, ErrUnsupportedMediaType
		}
	}

	return createMiddlewareChain(route.middlewares, route.handler), nil
}

func createMiddlewareChain(middlewares []MiddlewareFunc, source http.Handler) http.Handler {
	last := len(middlewares) - 1
	for index := last; index >= 0; index-- {
		source = middlewares[index](source)
	}

	return source
}

func (router *Router) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	p := request.URL.Path
	if uri := cleanPath(p); p != uri { // if request path contains some mistakes (/first//second/..///path)
		// reason: to avoid truncate #iamurltoo part
		newURL := *request.URL
		newURL.Path = p
		p = newURL.String()

		writer.WriteHeader(http.StatusMovedPermanently)
		writer.Header().Set("Location", p)
	}

	handler, err := router.match(p, request)
	if err != nil {
		switch err {
		case ErrMethodNotAllowed:
			writer.WriteHeader(http.StatusMethodNotAllowed)
			return

		case ErrHandlerNotFound:
			http.NotFound(writer, request)
			return

		case ErrUnsupportedMediaType:
			writer.WriteHeader(http.StatusUnsupportedMediaType)
			return
		}
	}

	handler = createMiddlewareChain(router.middlewares, handler)

	handler.ServeHTTP(writer, request)
}

// AddMiddleware adds given middleware to the middleware container
func (router *Router) AddMiddleware(middlewareFunc MiddlewareFunc) {
	router.middlewares = append(router.middlewares, middlewareFunc)
}
