package dto

import "time"

// CreateEventDto is a struct used for mapping from raw request data for create event function to event
type CreateEventDto struct {
	CreatorUserID *int       `validate:"required"` // since we have no authentication/authorization
	Name          *string    `validate:"required"`
	Date          *time.Time `validate:"required, after=now"`
}

// UpdateEventDto is a struct used for mapping from raw request data for create event function to event
type UpdateEventDto struct {
	ID             *int `validate:"required"`
	ModifierUserID *int `validate:"required"`
	Name           *string
	Date           *time.Time
}
