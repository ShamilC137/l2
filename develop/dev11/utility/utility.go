package utility

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/schema"
	"github.com/spf13/viper"
	"io"
	"log"
	"net/http"
	"reflect"
	"unsafe"
)

// LogFatal calls log.fatal if given error != nil
func LogFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func getLowestValue(val reflect.Value) reflect.Value {
	for kind := val.Kind(); kind == reflect.Pointer || kind == reflect.Interface; kind = val.Kind() {
		val = val.Elem()
	}
	return val
}

// Merge merges two structures into one (lhs) if the fields have a default value. lhs must be a pointer,
// otherwise the function has no effect. Pointers can be cast to their values (rhs), but not vice versa (e.g. lhs: field1 int, rhs: field1 *int)
func Merge(lhs, rhs any) (any, error) {
	lhsVal := getLowestValue(reflect.ValueOf(lhs))
	rhsVal := getLowestValue(reflect.ValueOf(rhs))

	if kind := lhsVal.Kind(); kind == reflect.Invalid {
		return nil, NewBadTypeError(kind.String())
	}

	if kind := rhsVal.Kind(); kind == reflect.Invalid {
		return nil, NewBadTypeError(kind.String())
	}

	if rhsVal.IsZero() {
		return lhs, nil
	}

	lhsType := lhsVal.Type()

Loop:
	for index, nFields := 0, lhsVal.NumField(); index < nFields; index++ {
		target := lhsVal.Field(index)
		if !target.CanSet() {
			continue
		}

		if target.IsZero() {
			field := rhsVal.FieldByName(lhsType.Field(index).Name)
			if !field.IsValid() || field.IsZero() {
				continue
			}

			targetType := target.Type()
			for !field.CanConvert(targetType) {
				if field.Kind() == reflect.Invalid {
					continue Loop // breaks current loop and skips target.Set
				}

				if kind := field.Kind(); kind == reflect.Pointer || kind == reflect.Interface {
					field = field.Elem()
				} else {
					continue Loop // breaks current loop and skips target.Set
				}
			}

			target.Set(field)
		}
	}

	return lhs, nil
}

// WriteErrorAsJSON writes error using given writer as json (like {"error": "err.Error()" } and sets given status code
func WriteErrorAsJSON(err error, writer http.ResponseWriter, statusCode int) {
	writer.WriteHeader(statusCode)
	_, respErr := io.WriteString(writer, fmt.Sprintf("{\"error\": \"%s\"}", err.Error()))
	LogFatal(respErr)
}

// WriteAsJSON writes given object as json to the given writer (like {"result": json.Marshal(value) }) and sets given status code
func WriteAsJSON(value any, writer http.ResponseWriter, statusCode int) {
	data, err := json.Marshal(value)
	if err != nil {
		WriteErrorAsJSON(err, writer, 500)
		return
	}

	writer.WriteHeader(statusCode)
	_, respErr := io.WriteString(writer, fmt.Sprintf("\"result\": %s", *(*string)(unsafe.Pointer(&data))))
	LogFatal(respErr)
}

// ExtractValue extracts values to the value of specified type ('dto') from the given 'form' using 'decoder' and then calls Merge(new(Struct), decodedValue)
func ExtractValue[Dto, Struct any](decoder *schema.Decoder, form map[string][]string) (*Struct, *HTTPError) {
	data := new(Dto)
	err := decoder.Decode(data, form)
	if err != nil {
		return nil, NewHTTPError(400, err.Error())
	}

	err = Validate(data)
	if err != nil {
		return nil, NewHTTPError(400, err.Error())
	}

	value := new(Struct)
	_, err = Merge(value, data)
	if err != nil {
		return nil, NewHTTPError(503, err.Error())
	}

	return value, nil
}

// ReadConfiguration reads configurations with the given 'key' from the configured file (viper config file must be present before any calls)
// and unmarshal it to the new(T)
func ReadConfiguration[T any](key string) (config *T, err error) {
	config = new(T)
	err = viper.UnmarshalKey(key, config)
	if err != nil {
		config = nil
	}

	return
}
