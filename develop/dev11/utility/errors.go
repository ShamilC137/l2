package utility

import (
	"fmt"
	"strings"
)

type cachedBuilt struct {
	cached string
}

func (cache *cachedBuilt) GetOrCreate(format string, args ...any) string {
	if len(cache.cached) == 0 {
		cache.cached = fmt.Sprintf(format, args...)
	}
	return cache.cached
}

// validator errors

// ValidationError stores information about field constraint
type ValidationError struct {
	flag       string
	flagValue  string
	field      string
	fieldValue any
	*cachedBuilt
}

// NewValidationError returns new ValidationError object
func NewValidationError(flag, flagValue, field string, fieldValue any) *ValidationError {
	return &ValidationError{flag, flagValue, field, fieldValue, &cachedBuilt{}}
}

func (err *ValidationError) Error() string {
	return err.GetOrCreate("validation failed for flag: '%s' with value: '%s'\nfor field: '%s' with value: '%v'", err.flag, err.flagValue, err.field, err.fieldValue)
}

// MalformedFieldFlagError stores information about errors in field tags
type MalformedFieldFlagError struct {
	flagAndValue string
	message      string
	*cachedBuilt
}

// NewMalformedFieldFlagError returns new MalformedFieldFlagError
func NewMalformedFieldFlagError(flagAndValue, message string) *MalformedFieldFlagError {
	return &MalformedFieldFlagError{strings.Trim(flagAndValue, " "), message, &cachedBuilt{}}
}

func (err *MalformedFieldFlagError) Error() string {
	return err.GetOrCreate("field flag tag is malformed: '%s', cause: %s", err.flagAndValue, err.message)
}

// WrongTypeError stores information about validated type if type is not supported for given flag
type WrongTypeError struct {
	actualType   string
	expectedType string
	*cachedBuilt
}

// NewWrongTypeError returns new WrongTypeError object
func NewWrongTypeError(actualType, expectedType string) *WrongTypeError {
	return &WrongTypeError{actualType, expectedType, &cachedBuilt{}}
}

func (err *WrongTypeError) Error() string {
	return err.GetOrCreate("field have a wrong type. Expected type: '%s', actual type: '%s'", err.expectedType, err.actualType)
}

// WrongArgumentError stores information about bad arguments for specified flags
type WrongArgumentError struct {
	typ   string
	value string
	*cachedBuilt
}

// NewWrongArgumentError returns new WrongArgumentError object
func NewWrongArgumentError(typ, value string) *WrongArgumentError {
	return &WrongArgumentError{typ, value, &cachedBuilt{}}
}

func (err *WrongArgumentError) Error() string {
	return err.GetOrCreate("cannot construct value of type: '%s' from value: '%s'", err.typ, err.value)
}

// reflection errors

// BadTypeError is returned when given interface/pointer stores invalid value
type BadTypeError struct {
	kind string
	*cachedBuilt
}

// NewBadTypeError returns new BadTypeError object
func NewBadTypeError(kind string) *BadTypeError {
	return &BadTypeError{kind, &cachedBuilt{}}
}

func (err *BadTypeError) Error() string {
	return err.GetOrCreate("type must be not a interface or pointer. The lowest kind: %s", err.kind)
}

// http errors

// HTTPError contains information about HTTP error (message and status code)
type HTTPError struct {
	Code    int
	message string
	*cachedBuilt
}

// NewHTTPError returns new HTTPError object
func NewHTTPError(code int, message string) *HTTPError {
	return &HTTPError{code, message, &cachedBuilt{}}
}

func (err *HTTPError) Error() string {
	return err.GetOrCreate("error during request processing: %s", err.message)
}

// common errors

// InvalidArgumentError argument failed function validation
type InvalidArgumentError struct {
	argument string
	cause    string
	*cachedBuilt
}

// NewInvalidArgumentError returns new InvalidArgumentError object
func NewInvalidArgumentError(argument, cause string) *InvalidArgumentError {
	return &InvalidArgumentError{argument: argument, cause: cause, cachedBuilt: &cachedBuilt{}}
}

func (err *InvalidArgumentError) Error() string {
	return err.GetOrCreate("invalid argument for %s. Cause: %s", err.argument, err.cause)
}
