package utility

import (
	"reflect"
	"strings"
	"time"
)

// could be replaced with https://github.com/go-playground/validator

// required checks that type is zero
// after: checks that given date is after specified value; date format: YYYY-MM-DDThh:mm:ssZ
// 	special values:
//		- now - current date

type validatorFunc func(reflect.Value, string) (bool, error)

func toTime(field reflect.Value) (time.Time, bool) {
	val, ok := reflect.Indirect(field).Interface().(time.Time)
	return val, ok
}

func afterNowValidator(field reflect.Value, _ string) (bool, error) {
	timeValue, ok := toTime(field)
	if !ok {
		return false, NewWrongTypeError(field.Type().Name(), "time.Time")
	}

	return timeValue.After(time.Now()), nil
}

func afterDefaultValidator(field reflect.Value, arg string) (bool, error) {
	rhs, err := time.Parse(time.RFC3339, arg)
	if err != nil {
		return false, NewWrongArgumentError("time.Time", arg)
	}

	val, ok := toTime(field)
	if !ok {
		return false, NewWrongTypeError(field.Type().Name(), "time.Time")
	}

	return val.After(rhs), nil
}

func requiredDefaultValidator(value reflect.Value, _ string) (bool, error) {
	return !value.IsZero(), nil
}

type validators map[string]validatorFunc

// allows to register your own validator functions
var flags = map[string]validators{"required": {"default": validatorFunc(requiredDefaultValidator)},
	"after": {"now": validatorFunc(afterNowValidator), "default": validatorFunc(afterDefaultValidator)}}

// Validate validates given structure using 'validate' flag.
// Available flags:
// - required - object must be not zero
// - after - object must have type time.Time and field.After(flagValue) must be true. Special flag value: now - compares against now
func Validate(value any) error {
	reflValue := reflect.ValueOf(value)
	for kind := reflValue.Kind(); kind == reflect.Pointer || kind == reflect.Interface; kind = reflValue.Kind() {
		reflValue = reflValue.Elem()
	}

	if kind := reflValue.Kind(); kind == reflect.Invalid {
		return NewBadTypeError(kind.String())
	}

	reflType := reflValue.Type()
	for index, numFields := 0, reflType.NumField(); index < numFields; index++ {
		var validation string
		field := reflType.Field(index)
		if validation = field.Tag.Get("validate"); len(validation) == 0 { // no need for validation
			continue
		}

		for _, flagAndValue := range strings.Split(validation, ",") { // flag=validator key or its value (default validator case)
			pair := strings.Split(flagAndValue, "=")
			length := len(pair)
			if length > 2 || length < 1 {
				return NewMalformedFieldFlagError(flagAndValue, "too much values")
			}

			flag := strings.Trim(pair[0], " ")
			validators, found := flags[flag]
			if !found {
				return NewMalformedFieldFlagError(flagAndValue, "flag not found")
			}

			valid := false
			var err error
			fieldValue := reflValue.Field(index)
			var validatorValue string
			if length == 1 {
				valid, err = validators["default"](fieldValue, "")
			} else {
				validatorValue = strings.Trim(pair[1], " ")
				if validator, found := validators[validatorValue]; found {
					valid, err = validator(fieldValue, "")
				} else {
					valid, err = validators["default"](fieldValue, validatorValue)
				}
			}

			if err != nil {
				return NewMalformedFieldFlagError(flagAndValue, err.Error())
			}

			if !valid {
				return NewValidationError(flag, validatorValue, field.Name, fieldValue.Interface())
			}
		}
	}
	return nil
}
