package main

import (
	"Task11/datasource"
	"Task11/datasource/repo"
	"Task11/server"
	"Task11/utility"
	"context"
	"github.com/spf13/viper"
	"os"
	"os/signal"
	"sync"
)

/*
=== HTTP server ===

Реализовать HTTP сервер для работы с календарем. В рамках задания необходимо работать строго со стандартной HTTP библиотекой.
В рамках задания необходимо:
	1. Реализовать вспомогательные функции для сериализации объектов доменной области в JSON.
	2. Реализовать вспомогательные функции для парсинга и валидации параметров методов /create_event и /update_event.
	3. Реализовать HTTP обработчики для каждого из методов API, используя вспомогательные функции и объекты доменной области.
	4. Реализовать middleware для логирования запросов
Методы API: POST /create_event POST /update_event POST /delete_event GET /events_for_day GET /events_for_week GET /events_for_month
Параметры передаются в виде www-url-form-encoded (т.е. обычные user_id=3&date=2019-09-09).
В GET методах параметры передаются через queryString, в POST через тело запроса.
В результате каждого запроса должен возвращаться JSON документ содержащий либо {"result": "..."} в случае успешного выполнения метода,
либо {"error": "..."} в случае ошибки бизнес-логики.

В рамках задачи необходимо:
	1. Реализовать все методы.
	2. Бизнес логика НЕ должна зависеть от кода HTTP сервера.
	3. В случае ошибки бизнес-логики сервер должен возвращать HTTP 503. В случае ошибки входных данных (невалидный int например) сервер должен возвращать HTTP 400. В случае остальных ошибок сервер должен возвращать HTTP 500. Web-сервер должен запускаться на порту указанном в конфиге и выводить в лог каждый обработанный запрос.
	4. Код должен проходить проверки go vet и golint.
*/

// see README.md

const (
	routinesCount = 1
	configName    = "application_properties"
	configType    = "yaml"
	configPath    = "resources/"
)

func init() {
	viper.SetConfigName(configName)
	viper.SetConfigType(configType)
	viper.AddConfigPath(configPath)
	utility.LogFatal(viper.ReadInConfig())

	db, err := datasource.GetDatasourceInstance("datasource")
	utility.LogFatal(err)

	_, err = repo.GetUserRepo(db) // creates table if it does not exist
	utility.LogFatal(err)

	_, err = repo.GetEventRepo(db) // creates table if it does not exist
	utility.LogFatal(err)
}

func main() {
	defer func() {
		utility.LogFatal(datasource.CloseDatasource()) // no need for explicitly closing for all repos (all prepared statement die here)
	}()

	var shutdownSync = sync.WaitGroup{}
	shutdownSync.Add(routinesCount)

	ctx, cancel := context.WithCancel(context.Background())

	serv, err := server.NewHTTPServer()
	utility.LogFatal(err)
	go func() {
		utility.LogFatal(server.StartServe(ctx, serv))
		shutdownSync.Done()
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown)
	<-shutdown

	cancel()

	shutdownSync.Wait()
}
