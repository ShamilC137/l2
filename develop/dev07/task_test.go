package main

import (
	"testing"
	"time"
)

func BenchmarkOr(b *testing.B) {

	cases := []struct {
		name     string
		function func(...<-chan any) <-chan any
	}{{"or", or},
		{"reflectiveOr", reflectiveOr}}

	b.ResetTimer()
	for _, tc := range cases {
		b.Run(tc.name, func(b *testing.B) {
			sig := func(after time.Duration) <-chan interface{} {
				c := make(chan interface{})
				go func() {
					defer close(c)
					time.Sleep(after)
				}()

				return c
			}

			for counter := 0; counter < b.N; counter++ {
				<-or(sig(2*time.Second),
					sig(1*time.Second),
					sig(time.Millisecond*100),
					sig(time.Millisecond*10),
					sig(time.Millisecond*500),
					sig(time.Millisecond*50),
					sig(time.Second*2),
					sig(time.Millisecond*150),
					sig(time.Millisecond*75))
			}
		})
	}
}
