package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Specify duration")
		os.Exit(1)
	}

	val, err := time.ParseDuration(os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	time.Sleep(val)
	fmt.Println("Done")
}
