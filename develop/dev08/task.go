package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"syscall"
)

/*
=== Взаимодействие с ОС ===

Необходимо реализовать собственный шелл

встроенные команды: cd/pwd/echo/kill/ps
поддержать fork/exec команды
конвеер на пайпах

Реализовать утилиту netcat (nc) клиент
принимать данные из stdin и отправлять в соединение (tcp/udp)
Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var (
	errInvalidArgument = errors.New("invalid argument")
)

const (
	inputChanCap = 8
	initBufCap   = 128

	binPath = "/bin/"
)

type process struct {
	pid  int
	name string
	proc *os.Process
}

type shell struct {
	processes []*process
	guard     sync.Mutex

	path     string
	internal map[string]func(io.Writer, string) error

	binPath  string
	binaries map[string]struct{}

	shutdown chan os.Signal
}

func (s *shell) fileStat(path string) (os.FileInfo, string, error) {
	path = strings.Trim(path, " ")
	if len(path) == 0 {
		return nil, "", errInvalidArgument
	}

	if !filepath.IsAbs(path) {
		path = filepath.Join(s.path, path)

		var err error
		path, err = filepath.Abs(path)
		if err != nil {
			return nil, "", err
		}
	}

	stat, err := os.Stat(path)
	return stat, path, err
}

// TODO: handle that Open ignores case
func (s *shell) cd(out io.Writer, path string) error {
	stat, path, err := s.fileStat(path)
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	} else if !stat.IsDir() {
		_, _ = fmt.Fprintln(out, "Cannot open directory")
		return err
	}

	s.path = path
	return nil
}

func (s *shell) echo(out io.Writer, args string) error {
	_, err := fmt.Fprintln(out, args)
	if err != nil {
		return errInvalidArgument
	}
	return nil
}

func (s *shell) searchProcess(pid int) int {
	for index, proc := range s.processes {
		if proc.pid == pid {
			return index
		}
	}

	return -1
}

func (s *shell) kill(out io.Writer, args string) error {
	var nextInt bool
	var pid = -1
	sig := syscall.SIGINT
	for _, arg := range strings.Fields(args) {
		if nextInt {
			val, err := strconv.Atoi(arg)
			if err != nil {
				_, _ = fmt.Fprintln(out, err)
				return err
			}
			if val <= 0 || syscall.Signal(val) > syscall.SIGTERM {
				_, _ = fmt.Fprintln(out, "Unknown sig")
				return err
			}

			sig = syscall.Signal(val)
			nextInt = false
			continue
		}

		if arg[0] == '-' {
			if len(arg) == 1 || arg[1:] != "s" {
				_, _ = fmt.Fprintln(out, "-s flag expected")
				return errInvalidArgument
			}

			nextInt = true
			continue
		}

		if pid == -1 {
			val, err := strconv.Atoi(arg)
			if err != nil {
				_, _ = fmt.Fprintln(out, err)
				return err
			}

			pid = val
			continue
		}

		_, _ = fmt.Fprintln(out, "PID already set")
		return errInvalidArgument
	}

	s.guard.Lock()
	defer s.guard.Unlock()
	if s.processes[0].pid == pid {
		s.shutdown <- sig
	} else {
		index := s.searchProcess(pid)
		if index == -1 {
			_, _ = fmt.Fprintln(out, "Unknown PID")
			return errInvalidArgument
		}

		err := s.processes[index].proc.Kill()
		if err != nil {
			_, _ = fmt.Fprintln(out, err)
			return err
		}

		s.processes = removeFromArray(s.processes, index)
	}

	return nil
}

func (s *shell) ps(out io.Writer, _ string) error {
	_, err := fmt.Fprintln(out, "PID\tName")
	s.guard.Lock()
	defer s.guard.Unlock()
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	}

	for _, proc := range s.processes {
		_, err = fmt.Fprintf(out, "%d\t%s\n", proc.pid, proc.name)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *shell) pwd(out io.Writer, _ string) error {
	_, err := fmt.Fprintln(out, s.path)
	if err != nil {
		return err
	}
	return nil
}

func (s *shell) cat(out io.Writer, path string) error {
	stat, path, err := s.fileStat(path)
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	} else if !stat.Mode().IsRegular() {
		_, _ = fmt.Fprintln(out, "File must be a regular file")
		return errInvalidArgument
	}

	file, err := os.Open(path)
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	}

	_, err = io.Copy(out, file)
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	}

	return nil
}

func (s *shell) addBinaries(p string) {
	files, err := os.ReadDir(p)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, file := range files {
		if file.IsDir() {
			s.addBinaries(file.Name())
		}

		// FIXME: is file executable check (windows does not support FileMode.Type)

		name := strings.Split(file.Name(), ".")
		s.binaries[strings.Join(name[:len(name)-1], "")] = struct{}{}
	}
}

func removeFromArray[T any](slice []T, index int) []T {
	copy(slice[index:], slice[index+1:])
	return slice[:len(slice)-1]
}

// runProcess can return only when background = false
func (s *shell) runProcess(in io.Reader, out io.Writer, name string, args string, background bool) error {
	cmd := exec.Command(filepath.Join(s.binPath, name), strings.Split(args, " ")...)
	cmd.Stdin = in
	cmd.Stdout = out

	err := cmd.Start()
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	}

	var proc *process
	if background {
		s.guard.Lock()
		proc = &process{name: name, proc: cmd.Process, pid: cmd.Process.Pid}
		s.processes = append(s.processes, proc)
		s.guard.Unlock()
	}

	err = cmd.Wait()
	if err != nil {
		_, _ = fmt.Fprintln(out, err)
		return err
	}

	if background {
		s.guard.Lock()
		s.processes = removeFromArray(s.processes, s.searchProcess(proc.pid))
		s.guard.Unlock()
	}

	return nil
}

func (s *shell) runPipelineImpl(commands []string, background bool) {
	in := bytes.NewBuffer(make([]byte, 0, initBufCap))
	out := bytes.NewBuffer(make([]byte, 0, initBufCap))

	for _, c := range commands {
		err := s.runCommand(c, in, out, background)
		if err != nil {
			_, _ = io.Copy(os.Stdout, out)
			break
		}

		in.Reset()
		_, err = io.Copy(in, out)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	_, _ = io.Copy(os.Stdout, in)
}

func (s *shell) runPipeline(commands []string, background bool) {
	if background {
		go func() {
			s.runPipelineImpl(commands, background)
		}()
	} else {
		s.runPipelineImpl(commands, background)
	}
}

func newShell() *shell {
	s := new(shell)
	s.internal = map[string]func(io.Writer, string) error{
		"cd":   s.cd,
		"echo": s.echo,
		"kill": s.kill,
		"ps":   s.ps,
		"pwd":  s.pwd,
		"cat":  s.cat,
	}

	var err error
	s.path, err = os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	s.binaries = make(map[string]struct{})
	s.binPath = s.path + binPath
	s.addBinaries(s.binPath)

	s.processes = append(s.processes, &process{name: "myshell", pid: syscall.Getpid()})

	s.shutdown = make(chan os.Signal, 1)
	signal.Notify(s.shutdown)

	return s
}

func inputListener(ch chan string) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		data := scanner.Text()
		ch <- data
	}
	close(ch)
}

func parseBinNameAndArgs(input string) (string, string) {
	binName := input
	var args string
	if index := strings.IndexByte(input, 32); index > 0 {
		binName = input[:index]

		if len(input) > index+1 {
			args = input[index+1:]
		}
	}

	return binName, args
}

func (s *shell) runCommand(input string, in io.Reader, out io.Writer, background bool) error {
	binName, args := parseBinNameAndArgs(strings.TrimLeft(input, " "))
	if handler := s.internal[binName]; handler != nil {
		return handler(out, args)
	} else if _, found := s.binaries[binName]; found {
		return s.runProcess(in, out, binName, args, background)
	}
	fmt.Println("Unrecognized input")
	return errInvalidArgument
}

func isBackground(input string) (bool, int) {
	if len(input) == 0 {
		return false, -1
	}

	trimmed := strings.TrimRight(input, " ")
	last := len(trimmed) - 1
	if trimmed[last] == '&' {
		return true, last
	}
	return false, -1
}

func main() {
	inputChan := make(chan string, inputChanCap)
	go inputListener(inputChan)

	s := newShell()
Loop:
	for {
		fmt.Print(s.path + "$")
		select {
		case sig := <-s.shutdown:
			fmt.Println(sig)
			// all children killed by os
			break Loop
		case input, ok := <-inputChan:
			if !ok {
				break Loop
			}

			background, index := isBackground(input)

			split := strings.Split(input, "|")
			if len(split) == 1 {
				if background {
					input = input[:index]
					go func() {
						_ = s.runCommand(input, nil, os.Stdout, background)
					}()
				} else {
					_ = s.runCommand(input, os.Stdin, os.Stdout, background)
				}
			} else {
				s.runPipeline(split, background)
			}
		}
	}
}
