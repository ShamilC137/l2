package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

const (
	readTimeout = 5 * time.Second
)

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func parseNet(args []string) (string, string, string) {
	var ip string
	var port string
	protocol := "tcp"

	for _, arg := range args {
		if arg[0] == '-' {
			if len(arg) > 1 {
				if arg[1] != 'u' {
					fmt.Println("Only -u flag supported, but got: " + arg)
					os.Exit(1)
				}
				protocol = "udp"
				continue
			} else {
				fmt.Println("Flag value expected")
				os.Exit(1)
			}
		}

		if len(ip) == 0 {
			ip = arg
			continue
		}

		if len(port) == 0 {
			port = arg
			continue
		}

		fmt.Println("Unexpected token")
		os.Exit(1)
	}

	return ip, port, protocol
}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("You must specify host and port")
		os.Exit(1)
	}

	ip, port, protocol := parseNet(os.Args[1:])
	conn, err := net.Dial(protocol, fmt.Sprintf("%s:%s", ip, port))
	handleError(err)
	defer conn.Close()

	_, err = io.Copy(conn, os.Stdin)
	handleError(err)

	err = conn.SetReadDeadline(time.Now().Add(readTimeout))
	handleError(err)

	_, err = io.Copy(os.Stdout, conn)

	fmt.Println()
	if err != nil {
		if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
			return // ok
		}
		handleError(err)
	}
}
