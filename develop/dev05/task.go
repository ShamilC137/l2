package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

/*
=== Утилита grep ===

Реализовать утилиту фильтрации (man grep)

Поддержать флаги:
-A - "after" печатать +N строк после совпадения
-B - "before" печатать +N строк до совпадения
-C - "context" (A+B) печатать ±N строк вокруг совпадения
-c - "count" (количество строк)
-i - "ignore-case" (игнорировать регистр)
-v - "invert" (вместо совпадения, исключать)
-F - "fixed", точное совпадение со строкой, не паттерн
-n - "line num", печатать номер строки

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type properties uint8

const (
	after = properties(1) << iota
	before
	ctxt
	count
	ignoreCase
	invert
	fixed
	lineNum
)

var flags = map[string]properties{"A": after, "B": before, "C": ctxt, "c": count, "i": ignoreCase, "v": invert, "F": fixed, "n": lineNum}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	var file *os.File
	var pattern string
	var props properties
	var nextNumber bool
	var lastFlag properties
	var afterCount int
	var beforeCount int
	for _, arg := range os.Args[1:] {
		if nextNumber {
			val, err := strconv.Atoi(arg)
			handleError(err)

			switch {
			case lastFlag&after == after:
				afterCount = val
			case lastFlag&before == before:
				beforeCount = val
			case lastFlag&ctxt == ctxt:
				afterCount = val
				beforeCount = val
			}

			nextNumber = false
			continue
		}

		if arg[0] == '-' {
			if len(arg) == 1 {
				fmt.Println("Flag value not specified (after -)")
				os.Exit(1)
			}

			flag, found := flags[arg[1:]]
			if !found {
				fmt.Printf("Unknown flag: %s\n", arg)
				os.Exit(1)
			}

			if flag <= ctxt {
				nextNumber = true
				lastFlag = flag
			}

			props |= flag
			continue
		}

		if len(pattern) == 0 {
			pattern = arg
			continue
		}

		if file != nil {
			fmt.Printf("Unexpected token: %s\n", arg)
		}

		var err error
		file, err = os.Open(arg)
		handleError(err)

		stat, err := file.Stat()
		handleError(err)

		if !stat.Mode().IsRegular() {
			fmt.Println("File must be a regular file!")
			os.Exit(1)
		}
	}

	if nextNumber {
		fmt.Println("You must specify flag value!")
		os.Exit(1)
	}

	if len(pattern) == 0 || file == nil {
		fmt.Println("Not enough arguments! You must specify pattern and filename")
		os.Exit(1)
	}

	s, err := newStream(pattern, bufio.NewScanner(file), props, afterCount, beforeCount)
	handleError(err)

	if props&count == count {
		fmt.Println(s.count())
	} else {
		for _, value := range s.search() {
			fmt.Println(value)
		}
	}
}
