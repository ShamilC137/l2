package main

import (
	"bufio"
	"regexp"
	"strconv"
	"strings"
)

type stream struct {
	props properties

	generator func() (string, bool)
	matcher   func(string) bool

	lines   []string
	matched []int

	afterCount  int
	beforeCount int
}

func newStream(pattern string, scanner *bufio.Scanner, props properties, afterCount, beforeCount int) (*stream, error) {
	s := &stream{props: props, afterCount: afterCount, beforeCount: beforeCount}

	var matcher func(value string) bool
	if props&fixed == fixed {
		matcher = func(value string) bool {
			return value == pattern
		}
	} else {
		expr, err := regexp.Compile(pattern)
		if err != nil {
			return nil, err
		}

		matcher = func(value string) bool {
			return expr.MatchString(value)
		}
	}

	if props&invert == invert {
		s.matcher = func(value string) bool {
			return !matcher(value)
		}
	} else {
		s.matcher = matcher
	}

	s.generator = func() (string, bool) {
		if scanner.Scan() {
			return scanner.Text(), true
		}

		return "", false
	}

	if props&ignoreCase == ignoreCase {
		s.generator = func() (string, bool) {
			if scanner.Scan() {
				return strings.ToLower(scanner.Text()), true
			}

			return "", false
		}
	}

	return s, nil
}

func (s *stream) indexes() []string {
	result := make([]string, len(s.matched))
	for index, value := range s.matched {
		result[index] = strconv.Itoa(value)
	}
	return result
}

func (s *stream) context() []string {
	length := len(s.matched)
	result := make([]string, 0, length*(s.afterCount+s.beforeCount))

	prev := 0
	right := -1
	for index, end := 0, length-1; index < end; index++ {
		next := s.matched[index+1]
		current := s.matched[index]

		// check intersection
		if prev < right {
			prev = right + 1
		}

		// [begin; end]
		result = append(result, s.selectBefore(current, prev)...)

		afterIns, end := s.selectAfter(current+1, next)
		result = append(result, afterIns...)

		prev = current
		right = end
	}

	last := s.matched[length-1]
	if last > right {
		result = append(result, s.selectBefore(last, prev)...)
	}

	vals, _ := s.selectAfter(last+1, len(s.lines)-1)
	result = append(result, vals...)

	return result
}

func (s *stream) selectAfter(begin, stop int) ([]string, int) {
	if stop-begin < s.afterCount {
		return s.lines[begin : stop+1], stop
	}

	stop = len(s.lines) - 1
	if diff := begin + s.afterCount; diff < stop {
		stop = diff
	}

	return s.lines[begin : stop+1], stop
}

func (s *stream) selectBefore(begin, stop int) []string {
	if begin-stop < s.beforeCount {
		return s.lines[stop : begin+1]
	}

	stop = 0
	if diff := begin - s.beforeCount; diff > 0 {
		stop = diff
	}

	return s.lines[stop : begin+1]
}

func (s *stream) after() []string {
	length := len(s.matched)
	if length == 1 {
		res, _ := s.selectAfter(s.matched[0], len(s.lines)-1)
		return res
	}

	result := make([]string, 0, length*s.afterCount)
	prev := s.matched[0]
	for _, value := range s.matched[1:] {
		vals, _ := s.selectAfter(prev, value-1)
		result = append(result, vals...)
		prev = value
	}

	vals, _ := s.selectAfter(prev, len(s.lines)-1)
	return append(result, vals...)
}

func (s *stream) before() []string {
	result := make([]string, 0, len(s.matched)*s.beforeCount)
	prev := 0
	for _, value := range s.matched {
		result = append(result, s.selectBefore(value, prev)...)
		prev = value + 1
	}

	return result
}

func (s *stream) handleSearchResult() []string {
	if len(s.matched) == 0 {
		return nil
	}

	switch {
	case s.props&lineNum == lineNum:
		return s.indexes()
	case s.props&ctxt == ctxt:
		return s.context()
	default:
		if s.props&after == after {
			if s.props&before == before {
				return s.context()
			}

			return s.after()
		}

		if s.props&before == before {
			return s.before()
		}
	}

	result := make([]string, len(s.matched))
	for index, value := range s.matched {
		result[index] = s.lines[value]
	}

	return result
}

func (s *stream) search() []string {
	index := 0

	for value, ok := s.generator(); ok; value, ok = s.generator() {
		s.lines = append(s.lines, value)

		if s.matcher(value) {
			s.matched = append(s.matched, index)
		}

		index++
	}

	return s.handleSearchResult()
}

func (s *stream) count() int {
	counter := 0

	for value, ok := s.generator(); ok; value, ok = s.generator() {
		if s.matcher(value) {
			counter++
		}
	}

	return counter
}
