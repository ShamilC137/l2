package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

/*
=== Утилита wget ===

Реализовать утилиту wget с возможностью скачивать сайты целиком

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	if len(os.Args) < 2 {
		fmt.Println("You must specify URL")
		os.Exit(1)
	}

	url := strings.Trim(os.Args[1], " ")
	scheme := "http://"
	if strings.HasPrefix(url, "http") {
		index := strings.Index(url, "://")
		if index == -1 {
			fmt.Println("Broken url, scheme must be separated with '://'")
			os.Exit(1)
		}

		if len(url) <= index+3 {
			fmt.Println("Broken url, you must specify host")
		}

		scheme = url[:index+3]
		url = url[index+3:]
	}

	ips, err := net.LookupIP(url)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Printf("Resolved ips: %s", ips)

	schemeLength := len(scheme)
	buf := bytes.NewBuffer(make([]byte, 0, schemeLength+15)) // scheme + 255.255.255.255
	buf.WriteString(scheme)
	fmt.Println("Trying to download site")
	var resp *http.Response
	for _, ip := range ips {
		buf.WriteString(ip.String())
		resp, err = http.Get(buf.String())
		if err == nil {
			break
		}

		buf.Truncate(schemeLength)
	}

	if resp == nil {
		fmt.Println("Cannot download resource with given url")
		os.Exit(1)
	}

	defer resp.Body.Close()

	file, err := os.Create(filepath.Join(".", "index.html"))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Site downloaded and saved into index.html")
}
