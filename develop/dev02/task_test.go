package main

import "testing"

func TestUnpack(t *testing.T) {
	cases := []struct {
		name        string
		arg         string
		expectedRes string
		expectedErr error
	}{{"First", "a4bc2d5e", "aaaabccddddde", nil},
		{"Second", "abcd", "abcd", nil},
		{"Third", "", "", nil},
		{"Fourth", "45", "", errBadPattern},
		{"Fifth", "qwe\\4\\5", "qwe45", nil},
		{"Sixth", "qwe\\45", "qwe44444", nil},
		{"Seventh", "qwe\\\\5", "qwe\\\\\\\\\\", nil},
		{"Eighth", "a\\b", "", errBadPattern},
		{"Ninth", "\\\\10", "\\\\\\\\\\\\\\\\\\\\", nil}}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actualRes, actualErr := unpack(tc.arg)
			if actualErr != tc.expectedErr || actualRes != tc.expectedRes {
				t.Logf("Actual result: %s, actual error: %v\n", actualRes, actualErr)
				t.Errorf("Expected result: %s, expected error: %v", tc.expectedRes, tc.expectedErr)
			}
		})
	}
}
