package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var errBadPattern = errors.New("bad pattern")

func isDigit(rune rune) bool {
	return '0' <= rune && rune <= '9'
}

func writeTimes(builder *strings.Builder, number []rune, what rune) ([]rune, error) {
	if len(number) == 0 {
		return number, nil
	}

	if what == -1 {
		return number, errBadPattern
	}

	times, err := strconv.ParseUint(string(number), 10, 32)
	if err != nil {
		return number, err
	}

	if times == 0 {
		return number[:0], nil
	}

	times--
	for counter := uint64(0); counter < times; counter++ {
		builder.WriteRune(what)
	}

	return number[:0], nil
}

func unpack(pattern string) (result string, err error) {
	if len(pattern) == 0 {
		return
	}

	builder := strings.Builder{}
	builder.Grow(len(pattern)) // at least

	escape := false
	lastRune := rune(-1)
	number := make([]rune, 0, 1)
	for _, char := range pattern {
		switch {
		case unicode.IsLetter(char):
			if escape {
				err = errBadPattern
				return
			}

			if number, err = writeTimes(&builder, number, lastRune); err != nil {
				return
			}

			lastRune = char
			builder.WriteRune(char)

		case isDigit(char): // only latin digits
			if escape {
				builder.WriteRune(char)
				escape = false
				lastRune = char
				continue
			}

			number = append(number, char)

		case char == '\\':
			if number, err = writeTimes(&builder, number, lastRune); err != nil {
				return
			}

			if escape {
				builder.WriteRune('\\')
				escape = false
				lastRune = '\\'
				continue
			}

			escape = true
		default:
			err = errBadPattern
			return
		}
	}

	if number, err = writeTimes(&builder, number, lastRune); err != nil {
		return
	}

	result = builder.String()
	return
}

func main() {
	fmt.Println(unpack("\\4\\5"))
}
