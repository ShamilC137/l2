package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"time"
)

/*
=== Утилита telnet ===

Реализовать примитивный telnet клиент:
Примеры вызовов:
go-telnet --timeout=10s host port go-telnet mysite.ru 8080 go-telnet --timeout=3s 1.1.1.1 123

Программа должна подключаться к указанному хосту (ip или доменное имя) и порту по протоколу TCP.
После подключения STDIN программы должен записываться в сокет, а данные полученные и сокета должны выводиться в STDOUT
Опционально в программу можно передать таймаут на подключение к серверу (через аргумент --timeout, по умолчанию 10s).

При нажатии Ctrl+D программа должна закрывать сокет и завершаться. Если сокет закрывается со стороны сервера, программа должна также завершаться.
При подключении к несуществующему сервер, программа должна завершаться через timeout.
*/

func parseArgs(args []string) (host, port string, timeout time.Duration) {
	timeout = time.Second * 10

	for _, arg := range args {
		trimmed := strings.Trim(arg, " ")
		if strings.HasPrefix(trimmed, "--") {
			index := strings.IndexByte(arg, '=')
			if index == -1 || len(arg) <= index+1 {
				fmt.Println("Value of the timeout expected")
				os.Exit(1)
			}

			var err error
			timeout, err = time.ParseDuration(arg[index+1:])
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			continue
		}

		if len(host) == 0 {
			host = trimmed
			continue
		}

		if len(port) == 0 {
			port = trimmed
			continue
		}

		fmt.Println("Unexpected token")
		os.Exit(1)
	}

	if len(host) == 0 || len(port) == 0 {
		fmt.Println("You must specify host and port!")
		os.Exit(1)
	}

	return
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("You must specify host and port")
		os.Exit(1)
	}
	host, port, timeout := parseArgs(os.Args[1:])
	// for loop and retry to check timeout
	start := time.Now()
	var conn net.Conn
	for diff := time.Now().Sub(start); diff < timeout && conn == nil; diff = time.Now().Sub(start) {
		conn, _ = net.DialTimeout("tcp", fmt.Sprintf("%s:%s", host, port), timeout-diff) // ignore error here
	}

	if conn == nil {
		fmt.Printf("Cannot establish connection, timeout exceeded (%s)\n", time.Now().Sub(start))
		os.Exit(1)
	}
	fmt.Println("Connected")

	go func() {
		_, _ = io.Copy(conn, os.Stdin) // blocks until Ctrl+D (or Ctrl+Z on Windows) or conn.Close()
		_ = conn.Close()
	}()

	_, _ = io.Copy(os.Stdout, conn)
	_ = conn.Close()

	fmt.Println()
	fmt.Println("Connection closed")
}
